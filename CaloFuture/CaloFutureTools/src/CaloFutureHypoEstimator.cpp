/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "CaloFutureHypoEstimator.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiKernel/IRegistry.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureHypoEstimator
//
// 2010-08-18 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::HypoEstimator, "CaloFutureHypoEstimator" )

namespace LHCb::Calo {

  StatusCode HypoEstimator::finalize() {
    IIncidentSvc* inc = incSvc();
    if ( inc ) { inc->removeListener( this ); }
    return GaudiTool::finalize();
  }

  StatusCode HypoEstimator::initialize() {
    StatusCode    sc  = GaudiTool::initialize(); // must be executed first
    IIncidentSvc* inc = incSvc();
    if ( inc ) inc->addListener( this, IncidentType::BeginEvent );

    m_toCaloFuture.retrieve();
    auto& h2c = dynamic_cast<IProperty&>( *m_toCaloFuture );
    h2c.setProperty( "Seed", m_seed ? "true" : "false" ).ignore();
    h2c.setProperty( "PhotonLine", m_extrapol ? "true" : "false" ).ignore();
    h2c.setProperty( "AddNeighbors", m_neig ? "true" : "false" ).ignore();

    m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );

    clean();
    m_status = true;
    return sc;
  }

  //=============================================================================

  // ------------
  std::optional<double> HypoEstimator::data( const LHCb::CaloCluster& cluster, LHCb::Calo::Enum::DataType type ) const {
    if ( &cluster != m_cluster ) {
      clean();
      m_status = estimator( cluster );
      if ( !m_status ) {
        if ( counterStat->isQuiet() ) counter( "Cluster estimation failed" ) += 1;
        clean();
      }
    }
    auto it = m_data.find( type );
    if ( it != m_data.end() ) return it->second;
    return {};
  }

  // ------------
  std::optional<double> HypoEstimator::data( const LHCb::CaloHypo& hypo, LHCb::Calo::Enum::DataType type ) const {
    if ( &hypo != m_hypo ) {
      clean();
      m_status = estimator( hypo );
      if ( !m_status ) {
        if ( counterStat->isQuiet() ) counter( "Hypo estimation failed" ) += 1;
        clean();
      }
    }
    auto it = m_data.find( type );
    if ( it != m_data.end() ) return it->second;
    return {};
  }

  // ------------ FROM HYPO
  bool HypoEstimator::estimator( const LHCb::CaloHypo& hypo ) const {
    using namespace LHCb::Calo::Enum;
    m_hypo = &hypo;

    LHCb::CaloMomentum mom( &hypo );
    m_data[DataType::HypoE]  = mom.e();
    m_data[DataType::HypoEt] = mom.pt();
    m_data[DataType::HypoM] =
        mom.mass(); // for mergedPi0 hypothesis only (->for all clusters - toDo in CaloFutureMergedPi0Alg)
    // using extra-digits
    const LHCb::CaloHypo::Digits& digits = hypo.digits();

    // electron matching
    if ( !m_skipC ) {
      // LHCb::CaloFuture2Track::ITrHypoTable2D* etable = getIfExists<LHCb::CaloFuture2Track::ITrHypoTable2D> (m_emLoc);
      const auto* etable = m_tables->getTrHypoTable2D( m_emLoc );
      if ( etable ) {
        const auto range = etable->inverse()->relations( &hypo );
        if ( !range.empty() ) {
          m_data[DataType::ElectronMatch]   = range.front().weight();
          m_track[MatchType::ElectronMatch] = range.front();
          if ( range.front() ) {
            LHCb::ProtoParticle dummy;
            dummy.setTrack( range.front() );
            dummy.addToCalo( &hypo );
            // CaloFutureElectron->caloTrajectory must be after addToCalo
            if ( m_electron->set( dummy ) )
              m_data[DataType::TrajectoryL] = m_electron->caloTrajectoryL( CaloPlane::ShowerMax );
          }
        }
      } else if ( counterStat->isQuiet() )
        counter( "Missing " + m_emLoc ) += 1;

      // brem matching
      // LHCb::CaloFuture2Track::ITrHypoTable2D* btable = getIfExists<LHCb::CaloFuture2Track::ITrHypoTable2D> (m_bmLoc);

      if ( const auto* btable = m_tables->getTrHypoTable2D( m_bmLoc ); btable ) {
        const auto range = btable->inverse()->relations( &hypo );
        if ( !range.empty() ) {
          m_track[MatchType::BremMatch] = range.front();
          m_data[DataType::BremMatch]   = range.front().weight();
        }
      } else if ( counterStat->isQuiet() )
        counter( "Missing " + m_bmLoc ) += 1;
    }

    // NeutralID (warning : protect against infinite loop from CaloFuturePhotonIDAlg using this tools with DoD active)
    if ( !m_skipN ) {
      for ( auto&& [hypothesis, loc] : m_pidLoc ) {

        if ( const LHCb::CaloFuture2Track::IHypoEvalTable* tbl =
                 getIfExists<LHCb::CaloFuture2Track::IHypoEvalTable>( loc );
             tbl ) {
          m_idTable[hypothesis] = tbl;
        } else if ( counterStat->isQuiet() ) {
          counter( "Missing " + loc ) += 1;
        }
      }
      std::ostringstream type;
      type << hypo.hypothesis();
      std::string hypothesis = type.str();
      auto        it         = m_idTable.find( hypothesis );
      if ( const LHCb::CaloFuture2Track::IHypoEvalTable* idTable = ( it != m_idTable.end() ? it->second : nullptr );
           idTable ) {
        const auto& range = idTable->relations( &hypo );
        if ( !range.empty() ) m_data[DataType::NeutralID] = range.front().to();
      }
    }
    // link 2 cluster :
    const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo );

    // Prs info
    if ( !cluster ) {
      Warning( "Cluster point to NULL", StatusCode::SUCCESS ).ignore();
    } else {
      // Ecal seed
      if ( auto iseed = LHCb::ClusterFunctors::locateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                            LHCb::CaloDigitStatus::SeedCell );
           iseed != cluster->entries().end() ) {
        const LHCb::CaloDigit* seed   = iseed->digit();
        const LHCb::CaloCellID idseed = seed->cellID();
        double                 sum9   = 0.;
        double                 sum1   = 0.;
        // PrsE4
        std::array<double, 4> Prse4s = {};
        std::array<double, 9> Prse9  = {};

        for ( const auto& id : digits ) {

          if ( !id ) continue;
          LHCb::CaloCellID id2 = id->cellID();
          if ( abs( (int)( id )->cellID().row() - (int)idseed.row() ) < 2 &&
               abs( (int)( id )->cellID().col() - (int)idseed.col() ) < 2 ) {
            // Build sum1 and sum9
            //
            sum9 += id->e();
            if ( id->cellID().row() == idseed.row() && id->cellID().col() == idseed.col() ) sum1 = id->e();
            // Prs4
            //
            if ( id2.col() <= idseed.col() && id2.row() >= idseed.row() ) Prse4s[0] += ( id )->e();
            if ( id2.col() >= idseed.col() && id2.row() >= idseed.row() ) Prse4s[1] += ( id )->e();
            if ( id2.col() >= idseed.col() && id2.row() <= idseed.row() ) Prse4s[2] += ( id )->e();
            if ( id2.col() <= idseed.col() && id2.row() <= idseed.row() ) Prse4s[3] += ( id )->e();
            // Select the 3X3 cluster
            //
            int dcol     = (int)id2.col() - (int)idseed.col() + 1;
            int drow     = (int)id2.row() - (int)idseed.row() + 1;
            int indexPrs = drow * 3 + dcol;
            Prse9[indexPrs] += id->e();
          }
        }
        //    Build E19 and E49
        //
        double Prse4max =
            std::accumulate( Prse4s.begin(), Prse4s.end(), 0., []( double l, double r ) { return std::max( l, r ); } );
        m_data[DataType::PrsE4Max] = Prse4max;
        if ( sum9 > 0. ) {
          double sum9Inv           = 1. / sum9;
          m_data[DataType::PrsE49] = Prse4max * sum9Inv;
          m_data[DataType::PrsE19] = sum1 * sum9Inv;
        } else {
          m_data[DataType::PrsE49] = 0.;
          m_data[DataType::PrsE19] = 0.;
        }
        m_data[DataType::PrsE1] = Prse9[0];
        m_data[DataType::PrsE2] = Prse9[1];
        m_data[DataType::PrsE3] = Prse9[2];
        m_data[DataType::PrsE4] = Prse9[3];
        m_data[DataType::PrsE5] = Prse9[4];
        m_data[DataType::PrsE6] = Prse9[5];
        m_data[DataType::PrsE7] = Prse9[6];
        m_data[DataType::PrsE8] = Prse9[7];
        m_data[DataType::PrsE9] = Prse9[8];
      }
    }

    auto insert_if = [&]( DataType key, std::optional<double> val ) {
      if ( val ) m_data[key] = val.value();
    };

    // gamma/pi0 separation
    insert_if( DataType::isPhoton, m_GammaPi0->isPhoton( hypo ) );
    insert_if( DataType::isPhotonXGB, m_GammaPi0XGB->isPhoton( hypo ) );

    // 1- Ecal variables :
    for ( const auto& [key, val] : m_GammaPi0->inputDataMap() ) m_data[key] = val;
    //
    bool ok = ( cluster && estimator( *cluster, &hypo ) );

    // Estimator MUST be at the end (after all inputs are loaded)

    auto get_data = [&]( LHCb::Calo::Enum::DataType type ) {
      auto d = m_data.find( type );
      return d != m_data.end() ? d->second : 0.;
    };

// FIXME: C++20: remove pragmas
// designated initializers are part of C99 (so both clang and gcc support them)
// and C++20, but when using C++17, they generate a '-Wpedantic' warning.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

    auto obs = LHCb::Calo::Interfaces::INeutralID::Observables{.clmatch = get_data( DataType::ClusterMatch ),
                                                               .prse    = get_data( DataType::ToPrsE ),
                                                               .e19     = get_data( DataType::E19 ),
                                                               .hclecl  = get_data( DataType::Hcal2Ecal ),
                                                               .prse19  = get_data( DataType::PrsE19 ),
                                                               .prse49  = get_data( DataType::PrsE49 ),
                                                               .sprd    = get_data( DataType::Spread ),
                                                               .prse4mx = get_data( DataType::PrsE4Max ),
                                                               .prsm    = get_data( DataType::HypoPrsM ),
                                                               .spdm    = get_data( DataType::HypoSpdM )};
#pragma GCC diagnostic pop

    insert_if( DataType::isNotH, m_neutralID->isNotH( hypo, obs ) );
    insert_if( DataType::isNotE, m_neutralID->isNotE( hypo, obs ) );

    return ok;
  }

  // ------------ FROM CLUSTER
  bool HypoEstimator::estimator( const LHCb::CaloCluster& cluster, const LHCb::CaloHypo* fromHypo ) const {
    using namespace LHCb::Calo::Enum;
    m_cluster = &cluster;

    if ( cluster.entries().empty() ) {
      if ( counterStat->isQuiet() ) counter( "Empty cluster" ) += 1;
      return false;
    }
    if ( auto iseed = LHCb::ClusterFunctors::locateDigit( cluster.entries().begin(), cluster.entries().end(),
                                                          LHCb::CaloDigitStatus::SeedCell );
         iseed != cluster.entries().end() ) {

      const LHCb::CaloDigit* seed = iseed->digit();
      if ( !seed ) {
        if ( counterStat->isQuiet() ) counter( "Seed points to NULL" ) += 1;
        return false;
      }

      double eEcal               = cluster.e();
      m_data[DataType::ClusterE] = eEcal;
      //
      double          cSize        = m_ecal->cellSize( cluster.seed() );
      Gaudi::XYZPoint cCenter      = m_ecal->cellCenter( cluster.seed() );
      double          asX          = ( cluster.position().x() - cCenter.x() ) / cSize;
      double          asY          = ( cluster.position().y() - cCenter.y() ) / cSize;
      m_data[DataType::ClusterAsX] = asX;
      m_data[DataType::ClusterAsY] = asY;

      m_data[DataType::E1]     = seed->e();
      auto   it                = m_data.find( DataType::HypoE );
      double eHypo             = ( it != m_data.end() ) ? it->second : 0;
      m_data[DataType::E1Hypo] = eHypo > 0. ? ( seed->e() ) / eHypo : -1.;
      LHCb::CaloCellID sid     = seed->cellID();
      m_data[DataType::CellID] = sid.all();
      m_data[DataType::Spread] = cluster.position().spread()( 1, 1 ) + cluster.position().spread()( 0, 0 );
      // E4
      std::array<double, 4> e4s       = {0, 0, 0, 0};
      double                e9        = 0.;
      double                ee9       = 0.; // full cluster energy without fraction applied
      double                e2        = 0.;
      bool                  hasShared = false;
      int                   code      = 0.;
      int                   mult      = 0.;
      int                   nsat      = 0; // number of saturated cells
      for ( const auto& ie : cluster.entries() ) {
        const LHCb::CaloDigit* dig = ie.digit();
        if ( !dig ) continue;
        double ecel = dig->e() * ie.fraction();
        if ( ( ( ie.status() & LHCb::CaloDigitStatus::UseForEnergy ) != 0 ||
               ( ie.status() & LHCb::CaloDigitStatus::UseForCovariance ) != 0 ) &&
             m_ecal->isSaturated( dig->e(), dig->cellID() ) )
          nsat++;
        LHCb::CaloCellID id = dig->cellID();
        if ( id.area() != sid.area() || abs( (int)id.col() - (int)sid.col() ) > 1 ||
             abs( (int)id.row() - (int)sid.row() ) > 1 )
          continue;
        if ( id.col() <= sid.col() && id.row() >= sid.row() ) e4s[0] += ecel;
        if ( id.col() >= sid.col() && id.row() >= sid.row() ) e4s[1] += ecel;
        if ( id.col() >= sid.col() && id.row() <= sid.row() ) e4s[2] += ecel;
        if ( id.col() <= sid.col() && id.row() <= sid.row() ) e4s[3] += ecel;
        e9 += ecel;
        // new info
        ee9 += dig->e();
        mult++;
        if ( ie.status() & LHCb::CaloDigitStatus::SharedCell ) hasShared = true;
        if ( !( id == sid ) && ecel > e2 ) {
          e2     = ecel;
          int dc = (int)id.col() - (int)sid.col() + 1;
          int dr = (int)id.row() - (int)sid.row() + 1;
          code   = 3 * dr + dc;
        }
      }
      m_data[DataType::Saturation] = nsat;
      double e4max                 = 0;
      for ( auto ih = e4s.begin(); e4s.end() != ih; ++ih ) {
        if ( *ih >= e4max ) e4max = *ih;
      }

      code = mult * 10 + code;
      if ( hasShared ) code *= -1;
      m_data[DataType::ClusterCode] = code;
      m_data[DataType::ClusterFrac] = ( e9 > 0. ? e9 / ee9 : -1 );

      m_data[DataType::E4]  = e4max;
      m_data[DataType::E9]  = e9;
      m_data[DataType::E49] = ( e9 > 0. ? e4max / e9 : 0. );
      m_data[DataType::E19] = ( e9 > 0. ? seed->e() / e9 : -1. );

      double eHcal = m_toCaloFuture->energy( cluster, CaloCellCode::CaloIndex::HcalCalo );
      eHcal        = 0; // FIXME: for now, preserve counter output for backwards compatibility, and to clearly
                        //       indentify the origin of changes of the result
      m_data[DataType::ToHcalE]   = eHcal;
      m_data[DataType::Hcal2Ecal] = ( eEcal > 0 ) ? eHcal / eEcal : 0.;
    }

    // cluster-match chi2
    if ( !m_skipCl ) {
      const LHCb::CaloCluster* clus = &cluster;
      // special trick for split cluster : use full cluster matching
      if ( fromHypo && fromHypo->hypothesis() == LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 )
        clus = LHCb::CaloFutureAlgUtils::ClusterFromHypo( fromHypo, false ); // get the main cluster
      if ( const LHCb::CaloFuture2Track::IClusTrTable* ctable =
               getIfExists<LHCb::CaloFuture2Track::IClusTrTable>( m_cmLoc );
           ctable ) {
        const auto& range = ctable->relations( clus );
        if ( !range.empty() ) {
          m_track[MatchType::ClusterMatch] = range.front();
          m_data[DataType::ClusterMatch]   = range.front().weight();
        }
      } else if ( counterStat->isQuiet() )
        counter( "Missing " + m_cmLoc ) += 1;
    }
    return true;
  }

  void HypoEstimator::clean() const {
    m_data.clear();
    m_cluster = nullptr;
    m_hypo    = nullptr;
    m_track.clear();
  }
} // namespace LHCb::Calo
