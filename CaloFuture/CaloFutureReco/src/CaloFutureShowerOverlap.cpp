/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from std
#include <vector>
// from Gaudi
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// local
#include "CaloFutureShowerOverlap.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureShowerOverlap
//
// 2014-06-02 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloFutureShowerOverlap )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloFutureShowerOverlap::CaloFutureShowerOverlap( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer(
          name, pSvcLocator,
          {KeyValue{"InputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalRaw" )},
           KeyValue{"Detector", LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name )}},
          KeyValue{"OutputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalOverlap" )} ) {}

//=============================================================================
// Main execution
//=============================================================================
LHCb::CaloCluster::Container CaloFutureShowerOverlap::operator()( const LHCb::CaloCluster::Container& clusters,
                                                                  const DeCalorimeter&                det ) const {

  auto eT = LHCb::CaloDataFunctor::EnergyTransverse{&det};

  // create new container
  LHCb::CaloCluster::Container outputClusters;
  // update the version number (needed for serialization)
  outputClusters.setVersion( 2 );
  outputClusters.reserve( clusters.size() );
  // copy clusters to new container
  for ( const auto* cl : clusters ) { outputClusters.insert( new LHCb::CaloCluster( *cl ) ); }

  // sort clusters, as this algorithm depends on the ordering
  auto byET = [&eT]( const LHCb::CaloCluster* lhs, const LHCb::CaloCluster* rhs ) { return eT( lhs ) > eT( rhs ); };
  std::sort( outputClusters.begin(), outputClusters.end(), byET );

  // neglect overlap from/to low ET clusters
  auto last = std::partition_point(
      outputClusters.begin(), outputClusters.end(),
      [&eT, m = std::min( m_etMin, m_etMin2 )]( const LHCb::CaloCluster* c ) { return eT( c ) > m; } );
  auto last2 = std::partition_point(
      outputClusters.begin(), last,
      [&eT, m = std::max( m_etMin, m_etMin2 )]( const LHCb::CaloCluster* c ) { return eT( c ) > m; } );

  // loop over new clusters and correct them
  for ( auto i1 = outputClusters.begin(); i1 != last2; ++i1 ) {
    auto*                  cl1 = *i1; // simplify syntax in the following
    const LHCb::CaloCellID id1 = cl1->seed();
    for ( auto i2 = std::next( i1 ); i2 != last; ++i2 ) {
      auto*                  cl2 = *i2; // simplify syntax in the following
      const LHCb::CaloCellID id2 = cl2->seed();
      if ( id1.area() != id2.area() ) continue;
      if ( abs( int( id1.col() ) - int( id2.col() ) ) > m_dMin || abs( int( id1.row() ) - int( id2.row() ) ) > m_dMin )
        continue;

      // initial weights for shared cells
      for ( auto& e1 : cl1->entries() ) {
        for ( auto& e2 : cl2->entries() ) {
          if ( e1.digit()->cellID() == e2.digit()->cellID() ) {
            const auto totE = cl1->e() + cl2->e();
            e1.setFraction( cl1->e() / totE );
            e2.setFraction( cl2->e() / totE );
          }
        }
      }
      // tag the cluster position to have correct corrections
      const StatusCode sc = StatusCode{m_tagger->tagPosition( *cl1 ) && m_tagger->tagPosition( *cl2 )};
      if ( sc.isFailure() ) ++m_cluster_tagging_failed;
      // correct entry weight for shower overlap (assuming EM cluster)
      m_oTool->process( *cl1, *cl2, m_iter );
    }
  }
  return outputClusters;
}

//=============================================================================
