/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "CaloFutureLCorrection.h"

/** @file
 *  Implementation file for class : CaloFutureLCorrection
 *
 *  @date 2003-03-10
 *  @author Xxxx XXXXX xxx@xxx.com
 */

DECLARE_COMPONENT( CaloFutureLCorrection )

// ============================================================================
/** Standard constructor
 *  @see GaudiTool
 *  @see  AlgTool
 *  @param type tool type (?)
 *  @param name tool name
 *  @param parent  tool parent
 */
// ============================================================================
CaloFutureLCorrection::CaloFutureLCorrection( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : extends( type, name, parent ) {

  // define conditionName
  const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
  if ( uName.find( "ELECTRON" ) != std::string::npos ) {
    m_conditionName = "Conditions/Reco/Calo/ElectronLCorrection";
  } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
    m_conditionName = "Conditions/Reco/Calo/SplitPhotonLCorrection";
  } else if ( uName.find( "PHOTON" ) ) {
    m_conditionName = "Conditions/Reco/Calo/PhotonLCorrection";
  }
  m_counterPerCaloFutureAreaDeltaZ.reserve( k_numOfCaloFutureAreas );
  m_counterPerCaloFutureAreaGamma.reserve( k_numOfCaloFutureAreas );
  m_counterPerCaloFutureAreaDelta.reserve( k_numOfCaloFutureAreas );
  std::vector<std::string> caloAreas = {"Outer", "Middle", "Inner", "PinArea"};
  for ( auto i = 0; i < k_numOfCaloFutureAreas; i++ ) {
    m_counterPerCaloFutureAreaDeltaZ.emplace_back( this, "Delta(Z) " + caloAreas[i] );
    m_counterPerCaloFutureAreaGamma.emplace_back( this, "<gamma> " + caloAreas[i] );
    m_counterPerCaloFutureAreaDelta.emplace_back( this, "<delta> " + caloAreas[i] );
  }
}

// ============================================================================
StatusCode CaloFutureLCorrection::finalize() {
  m_hypos.clear();
  return CaloFutureCorrectionBase::finalize();
}
// ============================================================================
StatusCode CaloFutureLCorrection::initialize() {
  /// first initialize the base class
  StatusCode sc = CaloFutureCorrectionBase::initialize();
  if ( sc.isFailure() ) return sc;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Condition name : " << m_conditionName << endmsg;

  return sc;
}

// ============================================================================
StatusCode CaloFutureLCorrection::correct( LHCb::span<LHCb::CaloHypo* const>   hypos,
                                           std::optional<const_ref_range_type> wrap_ref_matching_tracks ) const {
  if ( wrap_ref_matching_tracks.has_value() && UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << "Relation table passed to L Correction. Not used here" << endmsg;
  }
  for ( auto* hypo : hypos ) {

    // check the Hypo
    const auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo->hypothesis() );
    if ( m_hypos.end() == h ) return Error( "Invalid hypothesis -> no correction applied", StatusCode::SUCCESS );

    // No correction for negative energy :
    if ( hypo->e() < 0. ) {
      ++m_counterSkipNegativeEnergyCorrection;
      continue;
    }

    // get cluster energy
    const LHCb::CaloCluster* MainCluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo, true );
    if ( !MainCluster ) {
      Error( "CaloCLuster* points to NULL!" ).ignore();
      continue;
    }

    const double energy = hypo->e();

    const auto& entries = MainCluster->entries();
    const auto  iseed =
        LHCb::ClusterFunctors::locateDigit( entries.begin(), entries.end(), LHCb::CaloDigitStatus::SeedCell );
    if ( entries.end() == iseed ) {
      Error( "The seed cell is not found !" ).ignore();
      continue;
    }

    // get the "area" of the cluster (where seed is)
    const LHCb::CaloDigit* seed = iseed->digit();
    if ( !seed ) {
      Error( "Seed digit points to NULL!" ).ignore();
      continue;
    }

    // Cell ID for seed digit
    const auto cellID = seed->cellID();
    int        area   = cellID.area();
    assert( area >= 0 && area <= 2 ); // TODO: comment assert out

    /** here all information is available
     *
     *  (1) Ecal energy in 3x3     :    energy
     *  ( ) Prs and Spd energies   :  ( not available )
     *  (3) weighted barycenter    :    xBar, yBar
     *  (4) Zone/Area in Ecal      :    area
     *  (5) SEED digit             :    seed
     *  (6) CellID of seed digit   :    cellID
     *  (7) Position of seed cell  :    seedPos
     */

    // Account for the tilt
    const auto                plane = m_det->plane( CaloPlane::Front ); // Ecal Front-Plane
    const LHCb::CaloPosition* pos   = hypo->position();

    const auto xg     = pos->x() - m_origin.X();
    const auto yg     = pos->y() - m_origin.Y();
    const auto normal = plane.Normal();
    const auto Hesse  = plane.HesseDistance();
    const auto z0     = ( -Hesse - normal.X() * pos->x() - normal.Y() * pos->y() ) / normal.Z();
    auto       zg     = z0 - m_origin.Z();

    // hardcoded inner offset (+7.5 mm)
    if ( area == 2 ) { zg += 7.5; }

    // Uncorrected angle
    const auto aaa = std::pow( xg, 2 ) + std::pow( yg, 2 );
    const auto tth = std::sqrt( aaa ) / zg;
    // double cth = mycos ( myatan2( std::sqrt(aaa) , zg ) ) ;
    auto cth = zg / std::sqrt( aaa + std::pow( zg, 2 ) );

    // Corrected angle

    const auto gamma0 = getCorrection( CaloFutureCorrection::gamma0, cellID );
    const auto delta0 = getCorrection( CaloFutureCorrection::delta0, cellID );

    // NB: gammaP(ePrs = 0) != 0, deltaP(ePrs = 0) != 0 and depend on cellID.area()
    // get gammaP and deltaP parameters (depending on cellID.area() for each cluster
    const auto gammaP = getCorrection( CaloFutureCorrection::gammaP, cellID, 0. );
    const auto deltaP = getCorrection( CaloFutureCorrection::deltaP, cellID, 0. );
    const auto g      = gamma0 - gammaP;
    const auto d      = delta0 + deltaP;

    // double alpha = Par_Al1[zon] - myexp( Par_Al2[zon] - Par_Al3[zon] * ePrs/Gaudi::Units::MeV );
    // double beta  = Par_Be1[zon] + myexp( Par_Be2[zon] - Par_Be3[zon] * ePrs/Gaudi::Units::MeV  );

    m_counterPerCaloFutureAreaGamma.at( area ) += g;
    m_counterPerCaloFutureAreaDelta.at( area ) += d;

    const auto tgfps = ( energy > 0.0 ? g * mylog( energy / Gaudi::Units::GeV ) + d : 0.0 );

    const auto bbb = ( 1. + tgfps * cth / zg );
    // cth = mycos( myatan2( tth , bbb ) ) ;
    cth = bbb / std::sqrt( std::pow( tth, 2 ) + std::pow( bbb, 2 ) );

    const auto dzfps = cth * tgfps;
    m_counterPerCaloFutureAreaDeltaZ.at( area ) += dzfps;
    m_counterDeltaZ += dzfps;

    /*
      info() << " ======= Z0  FRONT-PLANE = " << z0 << " " << zg << endmsg;
      ROOT::Math::Plane3D planeSM = m_det->plane(CaloPlane::ShowerMax); // Ecal Front-Plane
      info() << " ======= Z0  SHOWERMAX = " << -planeSM.HesseDistance() <<endmsg;
      ROOT::Math::Plane3D planeM = m_det->plane(CaloPlane::Middle); // Ecal Middle
      info() << " ======= Z0  MIDDLE = " << -planeM.HesseDistance() <<endmsg;
    */

    // Recompute Z position and fill CaloFuturePosition
    const auto zCor = z0 + dzfps;

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
      debug() << "Hypothesis :" << hypo->hypothesis() << endmsg;
      debug() << " ENE  " << hypo->position()->e() << " "
              << "xg " << xg << " "
              << "yg " << yg << endmsg;
      debug() << "zg " << pos->z() << " "
              << "z0 " << z0 << " "
              << "DeltaZ " << dzfps << " "
              << "zCor " << zCor << endmsg;
    }

    hypo->position()->setZ( zCor );
  }

  return StatusCode::SUCCESS;
}

// ============================================================================
