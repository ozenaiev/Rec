/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CLUSTERCOVARIANCEMATRIXTOOL_H
#define CLUSTERCOVARIANCEMATRIXTOOL_H 1
// Include files
#include "CaloFutureCorrectionBase.h"
#include "ICaloFutureClusterTool.h"

#include "CaloFutureUtils/CovarianceEstimator.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Counters.h"

/** @class FutureClusterCovarianceMatrixTool
 *         FutureClusterCovarianceMatrixTool.h
 *
 *  Concrete tool for calculation of covariance matrix
 *  for the whole cluster object
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

namespace CaloFutureCovariance {
  enum Parameters {
    Stochastic      = 0, // stochastig term     Cov(EE)_i +=  [ S  * sqrt(E_i_GeV) ]^2
    GainError       = 1, // constant term       Cov(EE)_i +=  [ G  * E_i           ]^2
    IncoherentNoise = 2, // noise (inc.) term   Cov(EE)_i +=  [ iN * gain_i        ]^2
    CoherentNoise   = 3, // noise (coh.) term   Cov(EE)_i +=  [ cN * gain_i        ]^2
    ConstantE       = 4, // additional term     Cov(EE) +=  [ cE               ]^2
    ConstantX       = 5, // additional term     Cov(XX) +=  [ cX               ]^2
    ConstantY       = 6, // additional term     Cov(XX) +=  [ cY               ]^2
    Last
  };
  constexpr int            nParams                = Last + 1;
  inline const std::string ParameterName[nParams] = {"Stochastic", "GainError", "IncoherentNoise", "CoherentNoise",
                                                     "ConstantE",  "ConstantX", "ConstantY"};
  inline const std::string ParameterUnit[nParams] = {"Sqrt(GeV)", "", "ADC", "ADC", "MeV", "mm", "mm"};
  typedef std::map<std::string, std::vector<double>> ParameterMap;
  typedef SimplePropertyRef<ParameterMap>            ParameterProperty;
} // namespace CaloFutureCovariance

class FutureClusterCovarianceMatrixTool : public extends<GaudiTool, ICaloFutureClusterTool> {
public:
  FutureClusterCovarianceMatrixTool( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode initialize() override;
  StatusCode operator()( LHCb::CaloCluster& cluster ) const override;

private:
  StatusCode setEstimatorParams_wrapper() {
    setEstimatorParams( false );
    return StatusCode::SUCCESS;
  }

  StatusCode getParamsFromOptions();
  StatusCode getParamsFromDB();
  void       setEstimatorParams( bool init = false );

  CovarianceEstimator                                 m_estimator;
  const DeCalorimeter*                                m_det = nullptr;
  Gaudi::Property<CaloFutureCovariance::ParameterMap> m_parameters{this, "Parameters"};
  Gaudi::Property<std::string>                        m_detData{this, "Detector"};
  Gaudi::Property<bool>                               m_useDB{this, "UseDBParameters", true};
  Gaudi::Property<std::string>                        m_conditionName{this, "ConditionName"};
  ToolHandle<CaloFutureCorrectionBase> m_dbAccessor = {this, "CorrectionBase", "CaloFutureCorrectionBase/DBAccessor"};
  std::map<unsigned int, bool>         m_source_is_db;

  mutable Gaudi::Accumulators::Counter<> m_parUpdate{this, "parameter updated"};

}; ///< end of class FutureClusterCovarianceMatrixTool
// ============================================================================
#endif // CLUSTERCOVARIANCEMATRIXTOOL_H
