/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// CaloFutureUtils
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// local
#include "CaloFutureSelectNeutralClusterWithTracks.h"

// ============================================================================
/** @file
 *
 *  implementation file for class CaloFutureSelectNeutralClusterWithTracks
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 26 Apr 2002
 */
// ============================================================================

DECLARE_COMPONENT( CaloFutureSelectNeutralClusterWithTracks )

// ============================================================================
/** @brief "select"  method (functor interface)
 *
 *  Cluster is considered to be "selected"
 *  if there are no reconstructed tracks with
 *  chi2 value for 2D-matching under the threshold value
 *
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelectNeutralClusterWithTracks::operator()( const LHCb::CaloCluster* cluster ) const {
  // check the cluster
  if ( !cluster ) {
    Warning( "CaloCluster* points to NULL!" ).ignore();
    return false;
  }

  // locate the table
  LHCb::CaloFuture2Track::IClusTrTable* table = getIfExists<LHCb::CaloFuture2Track::IClusTrTable*>( m_tableLocation );
  if ( !table ) return true;

  // get all relations with WEIGHT = 'chi2' under the threshold value
  const LHCb::CaloFuture2Track::IClusTrTable::Range range = table->relations( cluster, m_chi2cut, false );

  bool isSelected = range.empty();
  if ( isSelected ) { ++m_counter; }
  return isSelected;
}
