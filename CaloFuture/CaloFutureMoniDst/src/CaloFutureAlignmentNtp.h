/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREALIGNEMENTNTP_H
#define CALOFUTUREALIGNEMENTNTP_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "Event/ODIN.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
// #include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// List of Consumers dependencies
namespace {
  using Vertices = LHCb::RecVertices;
  using ODIN     = LHCb::ODIN;
  using Digits   = LHCb::CaloDigits;
  using Tracks   = LHCb::Tracks;
  using Protos   = LHCb::ProtoParticles;
} // namespace

//==============================================================================

/** @class CaloFutureAlignmentNtp CaloFutureAlignmentNtp.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */

class CaloFutureAlignmentNtp final
    : public Gaudi::Functional::Consumer<void( const Vertices&, const ODIN&, const Digits&, const Tracks&,
                                               const Protos& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  /// Standard constructor
  CaloFutureAlignmentNtp( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  void       operator()( const Vertices&, const ODIN&, const Digits&, const Tracks&, const Protos& ) const override;

  /// C++11 non-copyable idiom
  CaloFutureAlignmentNtp()                                = delete;
  CaloFutureAlignmentNtp( const CaloFutureAlignmentNtp& ) = delete;
  CaloFutureAlignmentNtp& operator=( const CaloFutureAlignmentNtp& ) = delete;

private:
  bool acceptTrack( const LHCb::Track* ) const;
  bool hypoProcessing( const LHCb::CaloHypo* hypo ) const;
  bool inArea( const LHCb::CaloHypo* hypo ) const;

  // Tools
  DeCalorimeter* m_calo = nullptr;
  std::string    m_vertLoc;

  ToolHandle<IFutureCounterLevel>                m_counterStat{"FutureCounterLevel"};
  ToolHandle<LHCb::Calo::Interfaces::IElectron>  m_caloElectron{"CaloFutureElectron", this};
  ToolHandle<IEventTimeDecoder>                  m_odin{"OdinTimeDecoder/OdinDecoder", this};
  ToolHandle<LHCb::Calo::Interfaces::IHypo2Calo> m_toSpd{"CaloFutureHypo2CaloFuture/CaloFutureHypo2Spd", this};
  ToolHandle<LHCb::Calo::Interfaces::IHypo2Calo> m_toPrs{"CaloFutureHypo2CaloFuture/CaloFutureHypo2Prs", this};
  ToolHandle<ITrackExtrapolator>                 m_extrapolator{"TrackRungeKuttaExtrapolator/Extrapolator", this};

  // Flags -- init
  Gaudi::Property<bool>             m_usePV3D{this, "UsePV3D", false};
  Gaudi::Property<std::vector<int>> m_tracks{this, "TrackTypes", {LHCb::Track::Types::Long}};

  // Flags -- global cuts
  Gaudi::Property<std::pair<double, double>> m_nSpd{this, "SpdMult", {0., 250.}};
  Gaudi::Property<std::pair<double, double>> m_nTrk{this, "nTracks", {0., 350.}};
  Gaudi::Property<std::pair<double, double>> m_nVtx{this, "nVertices", {-1., 999999.}};
  Gaudi::Property<bool>                      m_inArea{this, "inAreaAcc", true};

  // Flags -- hypo cuts
  Gaudi::Property<std::pair<double, double>> m_e{this, "EFilter", {0., 99999999.}};
  Gaudi::Property<std::pair<double, double>> m_et{this, "EtFilter", {150., 999999.}};
  Gaudi::Property<std::pair<double, double>> m_prs{this, "PrsFilter", {10., 1024}};
  Gaudi::Property<std::pair<double, double>> m_spd{this, "SpdFilter", {.5, 9999.}};

  // Flags
  Gaudi::Property<std::pair<double, double>> m_eop{this, "EoPFilter", {0.7, 1.3}};
  Gaudi::Property<std::pair<double, double>> m_dlle{this, "DLLeFilter", {-9999999., 9999999.}};
  Gaudi::Property<std::pair<double, double>> m_rdlle{this, "RichDLLeFilter", {0., 99999.}};
  Gaudi::Property<std::pair<double, double>> m_bMatch{this, "BremMatchFilter", {-99999999., 99999999.}};
  Gaudi::Property<std::pair<double, double>> m_eMatch{this, "ElectronMatchFilter", {-999999., 999999.}};
  Gaudi::Property<std::pair<double, double>> m_cMatch{this, "ClusterMatchFilter", {-999999., 999999.}};
  Gaudi::Property<std::pair<double, double>> m_mas{this, "MassFilter", {-99999., 99999.}};
  Gaudi::Property<std::pair<double, double>> m_dist{this, "BremEleDistFilter", {4., 99999.}};

  Gaudi::Property<bool>  m_pairing{this, "EmlectronPairing", true};
  Gaudi::Property<float> m_min{this, "DeltaMin", -150.};
  Gaudi::Property<float> m_max{this, "DeltaMax", +150.};
  Gaudi::Property<int>   m_thBin{this, "ThetaBin", 14};
  Gaudi::Property<int>   m_bin{this, "DeltaBin", 150};
  Gaudi::Property<bool>  m_brem{this, "ElectronWithBremOnly", false};
  Gaudi::Property<float> m_r{this, "SshapeRange", 0.7};
  Gaudi::Property<int>   m_b{this, "SshapeBin", 50};

  // Flags -- outputs
  Gaudi::Property<bool> m_histo{this, "Histo", true};
  Gaudi::Property<bool> m_tuple{this, "Tuple", true};
  Gaudi::Property<bool> m_profil{this, "Profile", true};
};
#endif // CALOFUTUREALIGNEMENTNTP_H
