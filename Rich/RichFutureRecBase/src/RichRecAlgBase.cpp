/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecAlgBase.cpp
 *
 *  Implementation file for RICH reconstruction algorithm base class : RichRecAlgBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2003-05-10
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecAlgBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase<Rich::Future::AlgBase>;
// ============================================================================

// ============================================================================
// Standard constructor
// ============================================================================
Rich::Future::Rec::AlgBase::AlgBase( const std::string& name, ISvcLocator* pSvcLocator )
    : Rich::Future::AlgBase( name, pSvcLocator ) //
    , Rich::Future::Rec::CommonBase<Rich::Future::AlgBase>( this ) {}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode Rich::Future::Rec::AlgBase::initialize() {
  // Initialise base class
  const auto sc = Rich::Future::AlgBase::initialize();
  // Common initialisation
  return ( sc.isSuccess() ? initialiseRichReco() : sc );
}
// ============================================================================

// ============================================================================
// Main execute method
// ============================================================================
StatusCode Rich::Future::Rec::AlgBase::execute() {
  // All algorithms should re-implement this method
  error() << "Default Rich::RecAlgBase::execute() called !!" << endmsg;
  return StatusCode::FAILURE;
}
// ============================================================================

// ============================================================================
// Finalise
// ============================================================================
StatusCode Rich::Future::Rec::AlgBase::finalize() {
  // Common finalisation
  const auto sc = finaliseRichReco();
  // Finalize base class
  return ( sc.isSuccess() ? Rich::Future::AlgBase::finalize() : sc );
}
// ============================================================================
