/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <tuple>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Kernel
#include "Kernel/RichDetectorType.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// The output data
    using OutData = std::tuple<GeomEffs::Vector, GeomEffsPerPDVector, SegmentPhotonFlags::Vector>;
  } // namespace

  /** @class GeomEffCKMassRings RichRichGeomEffCKMassRings.h
   *
   *  Computes the geometrical efficiencies for a set of track segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class GeomEffCKMassRings final : public MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&, //
                                                                    const CherenkovAngles::Vector&,        //
                                                                    const MassHypoRingsVector& ),          //
                                                           Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    GeomEffCKMassRings( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector& segments, //
                        const CherenkovAngles::Vector&        ckAngles, //
                        const MassHypoRingsVector&            massRings ) const override;
  };

} // namespace Rich::Future::Rec
