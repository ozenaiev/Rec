###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RichRecUtils
################################################################################
gaudi_subdir(RichRecUtils v1r0)

gaudi_depends_on_subdirs(Event/RecEvent
                         Rich/RichUtils)

find_package(Boost)
find_package(Vc)
find_package(ROOT)
find_package(Eigen)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Vc_INCLUDE_DIR} ${EIGEN_INCLUDE_DIRS})

gaudi_add_library(RichRecUtils
                  src/*.cpp
                  PUBLIC_HEADERS RichRecUtils
                  INCLUDE_DIRS Boost
                  LINK_LIBRARIES RecEvent RichUtils)

gaudi_add_dictionary(RichRecUtils
                     dict/RichRecUtilsDict.h
                     dict/RichRecUtilsDict.xml
                     INCLUDE_DIRS Boost
                     LINK_LIBRARIES RecEvent RichUtils RichRecUtils
                     OPTIONS "-U__MINGW32__ -DBOOST_DISABLE_ASSERTS")
