/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Utils
#include "RichFutureUtils/RichHypoData.h"
#include "RichRecUtils/RichPhotonSpectra.h"

namespace Rich::Future::Rec {

  /// Type for photon yield data
  using PhotonYields = Rich::Future::HypoData<float>;

  /// photon yield TES locations
  namespace PhotonYieldsLocation {
    /// Location in TES for the emitted photon spectra
    inline const std::string Emitted = "Rec/RichFuture/PhotonYields/Emitted";
    /// Location in TES for the signal photon spectra
    inline const std::string Signal = "Rec/RichFuture/PhotonYields/signal";
    /// Location in TES for the detectable photon spectra
    inline const std::string Detectable = "Rec/RichFuture/PhotonYields/Detectable";
  } // namespace PhotonYieldsLocation

  /// The number of bins in the photon spectra data
  constexpr unsigned int NPhotonSpectraBins = 5;

  /// Type for photon spectra data
  using PhotonSpectra = Rich::PhotonSpectra<float, NPhotonSpectraBins>;

  /// photon spectra TES locations
  namespace PhotonSpectraLocation {
    /// Location in TES for the emitted photon spectra
    inline const std::string Emitted = "Rec/RichFuture/PhotonSpectra/Emitted";
    /// Location in TES for the signal photon spectra
    inline const std::string Signal = "Rec/RichFuture/PhotonSpectra/signal";
    /// Location in TES for the detectable photon spectra
    inline const std::string Detectable = "Rec/RichFuture/PhotonSpectra/Detectable";
  } // namespace PhotonSpectraLocation

} // namespace Rich::Future::Rec
