/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOHYPONTP_H
#define CALOHYPONTP_H 1

#include "CaloInterfaces/ICalo2MCTool.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "Event/CaloHypo.h"
#include "Event/L0DUReport.h"
#include "Event/ODIN.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"

// List of Consumers dependencies
namespace {
  using ODIN     = LHCb::ODIN;
  using L0       = LHCb::L0DUReport;
  using Hypos    = LHCb::CaloHypo::Container;
  using Tracks   = LHCb::Tracks;
  using Vertices = LHCb::RecVertices;
} // namespace

// ============================================================================

class CaloHypoNtp final
    : public Gaudi::Functional::Consumer<void( const ODIN&, const L0&, const Hypos&, const Tracks&, const Vertices& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  CaloHypoNtp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const ODIN&, const L0&, const Hypos&, const Tracks&, const Vertices& ) const override;

private:
  ToolHandle<ICalo2MCTool>       m_2MC         = {"Calo2MCTool", this};
  ToolHandle<ICounterLevel>      m_counterStat = {"CounterLevel"};
  ToolHandle<ICaloHypoEstimator> m_estimator   = {"CaloHypoEstimator", this};

  Gaudi::Property<bool> m_extrapol{this, "Extrapolation", true};
  Gaudi::Property<bool> m_seed{this, "AddSeed", false};
  Gaudi::Property<bool> m_neig{this, "AddNeighbors", false};

  Gaudi::Property<std::pair<double, double>> m_et{this, "RangePt", {100., 15000.}};
  Gaudi::Property<std::pair<double, double>> m_e{this, "RangeE", {0., 5000000}};
  Gaudi::Property<std::pair<double, double>> m_spdM{this, "RangeSpdM", {0., 5000000.}};
  Gaudi::Property<std::pair<double, double>> m_prsE{this, "RangePrsE", {0., 9999.}};

  Gaudi::Property<std::vector<std::string>> m_hypos{this, "Hypos", {"Electrons", "Photons", "MergedPi0s"}};

  Gaudi::Property<bool> m_tupling{this, "Tupling", true};
  Gaudi::Property<bool> m_checker{this, "CheckerMode", false};
  Gaudi::Property<bool> m_print{this, "Printout", false};
  Gaudi::Property<bool> m_stat{this, "Statistics", true};
  Gaudi::Property<int>  m_mcID{this, "MCID", -99999999};
};
#endif // CALOHYPONTP_H
