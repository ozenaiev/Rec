/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file OTHitCleaner.h
 *
 *  Header file for class : Tf::OTHitCleaner
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2005-01-10
 */
//-----------------------------------------------------------------------------

#ifndef TFTOOLS_OTHitCleaner
#define TFTOOLS_OTHitCleaner 1

// STL
#include <functional>
#include <iterator>
#include <map>

// From gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// Tf
#include "TfKernel/IOTHitCleaner.h"

namespace Tf {

  //-----------------------------------------------------------------------------
  /** @class OTHitCleaner OTHitCleaner.h
   *
   *  OTHit cleaner tool
   *
   *  @author S. Hansmann-Menzemer, W. Houlsbergen, C. Jones, K. Rinnert
   *
   *  @date   2007-06-01
   *
   *  @todo Is it possible to have a common interface for OT and ST (other?) cleaning tools
   **/
  //-----------------------------------------------------------------------------
  class OTHitCleaner : public extends<GaudiTool, IOTHitCleaner> {

  private:
    /// Max cluster size job option
    Gaudi::Property<int> m_maxClusterSize{this, "MaxClusterSize", 5};

    /// Max module occupancy
    Gaudi::Property<double> m_maxModuleOcc{this, "MaxModuleOccupancy", 0.4};

  public:
    /// Standard Constructor
    OTHitCleaner( const std::string& type, const std::string& name, const IInterface* parent );

    /// Tool initialization
    StatusCode initialize() override;

    // Clean the given range of hits
    OTHits cleanHits( const OTHits::const_iterator begin, const OTHits::const_iterator end ) const override;

  private:
    /// remove clusters
    void removeClusters( OTHits& output ) const;

    /// remove hot modules
    OTHits removeHotModules( const OTHits::const_iterator begin, const OTHits::const_iterator end ) const;

  }; // OTHitCleaner

} // namespace Tf

#endif
