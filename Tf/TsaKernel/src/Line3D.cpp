/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TsaKernel/Line3D.h"

namespace Tf {
  namespace Tsa {

    Line3D createLine3D( const Line& xLine, const Line& yLine, const double zRef ) {
      Gaudi::XYZVector direction = Gaudi::XYZVector( xLine.m(), yLine.m(), 1. );
      direction                  = direction.Unit();
      Gaudi::XYZPoint point      = Gaudi::XYZPoint( xLine.value( zRef ), yLine.value( zRef ), zRef );
      return Line3D( point, direction );
    }

  } // namespace Tsa
} // namespace Tf
