###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PatVelo
################################################################################
gaudi_subdir(PatVelo v3r34)

gaudi_depends_on_subdirs(Det/VeloDet
                         Event/DigiEvent
                         Event/TrackEvent
                         GaudiAlg
                         Kernel/LHCbMath
                         Tf/PatKernel
                         Tf/TfKernel
                         Tf/TfTools
                         Tr/TrackInterfaces)

find_package(AIDA)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(PatVelo
                 src/*.cpp
                 INCLUDE_DIRS Event/DigiEvent Tf/PatKernel Tf/TfKernel Tr/TrackInterfaces AIDA
                 LINK_LIBRARIES VeloDetLib TrackEvent GaudiAlgLib LHCbMathLib)

