/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_ISTSPILLOVERTOOL_H
#define TRACKINTERFACES_ISTSPILLOVERTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// Forward declarations
namespace LHCb {
  class Node;
}

/** @class ISTSpilloverTool ISTSpilloverTool.h TrackInterfaces/ISTSpilloverTool.h
 *
 *
 *  @author Vincenzo Battista
 *  @date   2015-08-13
 */
struct ISTSpilloverTool : extend_interfaces<IAlgTool> {
  DeclareInterfaceID( ISTSpilloverTool, 2, 0 );

  /// Return PDF(ADC,costheta|central)/PDF(ADC|spillover) starting from a track node
  virtual double pdfRatio( const LHCb::Node* node ) const = 0;
};
#endif // TRACKINTERFACES_ISTSPILLOVERTOOL_H
