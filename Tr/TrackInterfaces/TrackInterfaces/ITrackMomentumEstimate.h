/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_ITRACKMOMENTUMESTIMATE_H
#define TRACKINTERFACES_ITRACKMOMENTUMESTIMATE_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// forward declarations
namespace LHCb {
  class State;
}

/** @class ITrackMomentumEstimate ITrackMomentumEstimate.h TrackInterfaces/ITrackMomentumEstimate.h
 *
 *
 *  @author Stephanie Hansmann-Menzemer
 *  @date   2007-10-30
 */
struct ITrackMomentumEstimate : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( ITrackMomentumEstimate, 1, 0 );

  // Estimate the momentum P of a State in T at ZAtMidT
  virtual StatusCode calculate( const LHCb::State* TState, double& qOverP, double& sigmaQOverP,
                                bool cubical = 0 ) const = 0;

  // Estimate the momentum P of a velo State and a State in T at ZAtMidT
  virtual StatusCode calculate( const LHCb::State* veloState, const LHCb::State* tState, double& qOverP,
                                double& sigmaQOverP, bool cubical = 0 ) const = 0;
};
#endif // TRACKINTERFACES_ITRACKMOMENTUMESTIMATE_H
