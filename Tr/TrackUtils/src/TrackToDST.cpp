/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// track interfaces
#include "Event/Track.h"

#include "TrackToDST.h"

namespace {
  static const std::map<std::string, LHCb::State::Location> s_theMap = {
      {"ClosestToBeam", LHCb::State::Location::ClosestToBeam},
      {"FirstMeasurement", LHCb::State::Location::FirstMeasurement},
      {"EndVelo", LHCb::State::Location::EndVelo},
      {"AtTT", LHCb::State::Location::AtTT},
      {"AtT", LHCb::State::Location::AtT},
      {"BegRich1", LHCb::State::Location::BegRich1},
      {"EndRich1", LHCb::State::Location::EndRich1},
      {"BegRich2", LHCb::State::Location::BegRich2},
      {"EndRich2", LHCb::State::Location::EndRich2},
      {"Spd", LHCb::State::Location::Spd},
      {"Prs", LHCb::State::Location::Prs},
      {"BegECal", LHCb::State::Location::BegECal},
      {"ECalShowerMax", LHCb::State::Location::ECalShowerMax},
      {"EndECal", LHCb::State::Location::EndECal},
      {"BegHCal", LHCb::State::Location::BegHCal},
      {"MidHCal", LHCb::State::Location::MidHCal},
      {"EndHCal", LHCb::State::Location::EndHCal},
      {"Muon", LHCb::State::Location::Muon},
      {"V0Vertex", LHCb::State::Location::V0Vertex}};

  std::vector<LHCb::State::Location> stringToLoc( const std::vector<std::string>& sCont ) {
    std::vector<LHCb::State::Location> loc;
    loc.reserve( sCont.size() );
    std::transform( sCont.begin(), sCont.end(), std::back_inserter( loc ),
                    []( const std::string& s ) { return s_theMap.at( s ); } );
    return loc;
  }
} // namespace

DECLARE_COMPONENT( TrackToDST )

TrackToDST::TrackToDST( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ) {
  // constructor
  declareProperty( "TracksInContainer", m_inputLocation = LHCb::TrackLocation::Default );
  declareProperty( "StoreAllStates", m_storeAllStates = false );

  // CRJ : Orignal list
  //   m_veloStrings = list_of("ClosestToBeam");
  //   m_longStrings = list_of("ClosestToBeam")("FirstMeasurement")("BegRich1")("BegRich2")("V0Vertex");
  //   m_tStrings = list_of("FirstMeasurement")( "BegRich2");
  //   m_downstreamStrings = list_of("BegRich1")("FirstMeasurement")("BegRich2")("V0Vertex");
  //   m_upstreamStrings = list_of("ClosestToBeam")("FirstMeasurement")("BegRich1");
  //   m_muonStrings     = list_of("ClosestToBeam")("BegRich1")("BegRich2")("Muon");

  // (Slightly) reduced list
  declareProperty( "veloStates", m_veloStrings = {"ClosestToBeam"} );
  declareProperty( "longStates", m_longStrings = {"ClosestToBeam", "FirstMeasurement", "BegRich2", "V0Vertex"} );
  declareProperty( "TTrackStates", m_tStrings = {"FirstMeasurement", "BegRich2"} );
  declareProperty( "downstreamStates", m_downstreamStrings = {"FirstMeasurement", "BegRich2", "V0Vertex"} );
  declareProperty( "upstreamStates", m_upstreamStrings = {"ClosestToBeam", "FirstMeasurement"} );
  declareProperty( "muonStates", m_muonStrings = {} );
}

StatusCode TrackToDST::initialize() {
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize", sc ); }

  m_veloStates       = stringToLoc( m_veloStrings );
  m_longStates       = stringToLoc( m_longStrings );
  m_tStates          = stringToLoc( m_tStrings );
  m_downstreamStates = stringToLoc( m_downstreamStrings );
  m_upstreamStates   = stringToLoc( m_upstreamStrings );
  m_muonStates       = stringToLoc( m_muonStrings );
  return sc;
}

StatusCode TrackToDST::execute() {
  LHCb::Tracks* inCont = get<LHCb::Tracks>( m_inputLocation );
  // loop
  for ( auto& trk : *inCont ) {

    // remove the necessary States on the Track
    if ( !m_storeAllStates ) {
      // done in an ugly way for now - will be easier with the new
      // jobOptions parser
      const LHCb::Track::Types type = trk->type();
      switch ( type ) {
      case LHCb::Track::Types::Velo:
        cleanStates( *trk, m_veloStates );
        break;
      case LHCb::Track::Types::VeloR:
        cleanStates( *trk, m_veloStates );
        break;
      case LHCb::Track::Types::Long:
        cleanStates( *trk, m_longStates );
        break;
      case LHCb::Track::Types::Upstream:
        cleanStates( *trk, m_upstreamStates );
        break;
      case LHCb::Track::Types::Downstream:
        cleanStates( *trk, m_downstreamStates );
        break;
      case LHCb::Track::Types::Ttrack:
        cleanStates( *trk, m_tStates );
        break;
      case LHCb::Track::Types::Muon:
        cleanStates( *trk, m_muonStates );
        break;
      default:
        Warning( format( "Unknown track type %i", type ), StatusCode::SUCCESS, 1 ).ignore();
        break;
      } // switch
    }   // if
    // set the appropriate flag!
    trk->setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );

  } // iterT

  return StatusCode::SUCCESS;
}

void TrackToDST::cleanStates( LHCb::Track& aTrack, const SLocations& loc ) const {
  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "Analysing Track key=" << aTrack.key() << " type=" << aTrack.type() << " : " << aTrack.states().size()
              << " States at : z =";
    for ( const auto& s : aTrack.states() ) {
      if ( s ) verbose() << " (" << s->z() << " " << s->location() << ")";
    }
    verbose() << endmsg;
  }

  std::vector<LHCb::State*> tempCont;
  for ( const auto& l : loc ) {
    const LHCb::State* state = aTrack.stateAt( l );
    if ( state ) {
      tempCont.push_back( new LHCb::State{*state} );
    } else if ( l != LHCb::State::Location::V0Vertex ) {
      Warning( "Failed to find state - more info in DEBUG", StatusCode::SUCCESS, 1 ).ignore();
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        debug() << "Missing state at " << l << " on track " << aTrack.key() << " of type " << aTrack.type() << endmsg;
      }
    }
  } // loca

  aTrack.clearStates();
  aTrack.addToStates( tempCont );
}
