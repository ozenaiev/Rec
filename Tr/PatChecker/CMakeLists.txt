###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PatChecker
################################################################################
gaudi_subdir(PatChecker v3r16p1)

gaudi_depends_on_subdirs(Event/DigiEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/PhysEvent
                         GaudiAlg
                         GaudiKernel
                         Kernel/MCInterfaces
                         Tf/PatKernel
                         Tf/TfKernel)

find_package(AIDA)
find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(PatChecker
                 src/*.cpp
                 INCLUDE_DIRS AIDA GSL Event/DigiEvent Kernel/MCInterfaces Tf/PatKernel Tf/TfKernel
                 LINK_LIBRARIES GSL LinkerEvent MCEvent PhysEvent GaudiAlgLib GaudiKernel)

