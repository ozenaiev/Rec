/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTTRACKMONITOR_H
#define UTTRACKMONITOR_H 1

// Include files

// from Gaudi
#include "Event/Track.h"
#include "Event/UTCluster.h"
#include "TrackMonitorBase.h"

namespace LHCb {
  class LHCbID;
}

/** @class UTTrackMonitor UTTrackMonitor.h "TrackCheckers/UTTrackMonitor"
 *
 * Class for UT track monitoring
 *  @author M. Needham.
 *  @date   16-1-2009
 */

class UTTrackMonitor : public TrackMonitorBase {

public:
  /** Standard construtor */
  UTTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm execute */
  StatusCode execute() override;

private:
  void fillHistograms( const LHCb::Track& track, const LHCb::UTClusters& fullclusters,
                       const std::vector<LHCb::LHCbID>& itIDs, const std::string& type ) const;

  double m_xMax;
  double m_yMax;

  Gaudi::Property<double> m_refZ{this,
                                 "ReferenceZ",
                                 2500.0,
                                 [=]( auto& ) {
                                   // guess that the size of the histograms at ref is ~ 0.35 by 0.3
                                   this->m_xMax = 0.35 * this->m_refZ / Gaudi::Units::cm;
                                   this->m_yMax = 0.3 * this->m_refZ / Gaudi::Units::cm;
                                 },
                                 Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                 "midpoint of UT"};

  Gaudi::Property<unsigned int> m_minNumUTHits{this, "minNumUTHits", 2u};
  Gaudi::Property<std::string>  m_clusterLocation{this, "InputData", LHCb::UTClusterLocation::UTClusters};
};

#endif // UTTRACKMONITOR_H
