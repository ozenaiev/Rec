/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKRESCHECKER_H
#define TRACKRESCHECKER_H 1

#include "TrackCheckerBase.h"

#include "Linker/LinkedTo.h"

#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"

#include "GaudiAlg/Consumer.h"

#include <map>
#include <string>

class ITrackProjectorSelector;
class IHistoTool;

namespace LHCb {
  class State;
  class OTMeasurement;
} // namespace LHCb

/** @class TrackResChecker TrackResChecker.h
 *
 * Class for track monitoring
 *  @author M. Needham.
 *  @date   6-5-2007
 */

class TrackResChecker
    : public Gaudi::Functional::Consumer<void( const LHCb::Tracks&, const LHCb::MCParticles&, const LHCb::LinksByKey& ),
                                         Gaudi::Functional::Traits::BaseClass_t<TrackCheckerBase>> {

public:
  /** Standard constructor */
  TrackResChecker( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  void operator()( const LHCb::Tracks&, const LHCb::MCParticles&, const LHCb::LinksByKey& ) const override;

  /** Algorithm finalize */
  StatusCode finalize() override;

private:
  void resolutionHistos( const IHistoTool& histotool, const LHCb::Track& track, const LHCb::MCParticle& mcPart ) const;

  void pullplots( const IHistoTool& histotool, const LHCb::State& trueState, const LHCb::State& recState,
                  const std::string& location ) const;

  void checkAmbiguity( const IHistoTool& histotool, const LHCb::Track& track, const LHCb::MCParticle& mcPart ) const;

  void plotsByMeasType( const IHistoTool& histotool, const LHCb::Track& track, const LHCb::MCParticle& mcPart ) const;

  const IHistoTool* createHistoTool( const std::string& name ) const;

private:
  Gaudi::Property<bool> m_plotsByMeasType{this, "PlotsByMeasType", false};

  typedef LinkedTo<LHCb::MCParticle> OTLinks;
  mutable OTLinks                    m_otLinker{0, 0, ""};
  Gaudi::Property<unsigned int>      m_minToCountAmb{this, "MinToCountAmb", 8};
  Gaudi::Property<bool>              m_checkAmbiguity{this, "CheckAmbiguity", false};
  Gaudi::Property<double>            m_minAmbDist{this, "MinAmbDist", 0.3 * Gaudi::Units::mm};

  ITrackProjectorSelector*                 m_projectorSelector;
  typedef std::map<int, const IHistoTool*> HistoToolMap;
  HistoToolMap                             m_histoTools;
};

#endif // TRACKRESCHECKER_H
