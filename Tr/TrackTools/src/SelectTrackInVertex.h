/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_SelectTrackInVertex_H
#define TRACKTOOLS_SelectTrackInVertex_H

/** @class SelectTrackInVertex SelectTrackInVertex.h
 *
 *  Selector that returns true if a track is found in a collection of tracks belonging to a vertex (LHCb::RecVertex,
 * e.g. the PV)
 *
 *  Parameters:
 *
 *  - VertexContainer: Container of vertices
 *
 *  @author Michel De Cian
 *  @date   2015-09-18
 */

#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
// base class
#include "Event/RecVertex.h"
#include "TrackSelectorBase.h"

class SelectTrackInVertex : public extends<TrackSelectorBase, IIncidentListener> {

public:
  /// constructor
  using extends::extends;

  StatusCode initialize() override;

  /** Returns if the given track is selected or not
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return boolean indicating if the track is selected or not
   *  @retval true  Track is selected
   *  @retval false Track is rejected
   */
  bool accept( const LHCb::Track& aTrack ) const override;

  void handle( const Incident& incident ) override;

private:
  /// Get all tracks belonging to all vertices and put them in a container
  void getTracksFromVertices() const;

  Gaudi::Property<std::string> m_vertexContainerName{this, "VertexContainer", LHCb::RecVertexLocation::Primary};
  mutable bool                 m_newEvent;
  mutable std::vector<const LHCb::Track*> m_tracks;
};

#endif // TRACKTOOLS_SelectTrackInVertex_H
