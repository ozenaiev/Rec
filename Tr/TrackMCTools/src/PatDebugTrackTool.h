/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKMCTOOLS_PATDEBUGTRACKTOOL_H
#define TRACKMCTOOLS_PATDEBUGTRACKTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "PatKernel/IPatDebugTrackTool.h" // Interface

/** @class PatDebugTrackTool PatDebugTrackTool.h TrackMCTools/PatDebugTrackTool.h
 *
 *
 *  @author Olivier Callot
 *  @date   2009-03-31
 */
class PatDebugTrackTool : public GaudiTool, virtual public IPatDebugTrackTool {
public:
  /// Standard constructor
  PatDebugTrackTool( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~PatDebugTrackTool(); ///< Destructor

  std::vector<int> keys( const LHCb::Track* track ) override;

protected:
private:
};
#endif // TRACKMCTOOLS_PATDEBUGTRACKTOOL_H
