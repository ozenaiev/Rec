/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/Cache.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "GaudiKernel/Service.h"
#include "boost/algorithm/string/erase.hpp"
#include "boost/algorithm/string/predicate.hpp"
#include "boost/algorithm/string/replace.hpp"
#include "boost/algorithm/string/split.hpp"
#include "boost/algorithm/string/trim.hpp"
#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"
#include <TInterpreter.h>
#include <fstream>
#include <set>
#include <vector>

/** @file  Factory.cpp
 *  @brief Definitions of non-templated functor factory functions.
 */

namespace {
  std::ostream& include( std::ostream& os, std::string_view header ) {
    if ( header.empty() ) { throw GaudiException{"Got empty header name", "FunctorFactory", StatusCode::FAILURE}; }
    os << "#include ";
    if ( header.front() == '<' && header.back() == '>' ) {
      os << header;
    } else {
      os << '"' << header << '"';
    }
    return os << '\n';
  }

  std::string make_default_cppname( std::string cppname ) {
    if ( boost::algorithm::starts_with( cppname, "ToolSvc." ) ) { cppname.erase( 0, 8 ); }
    std::replace_if(
        cppname.begin(), cppname.end(), []( char c ) { return c == '.' || c == ' '; }, '_' );
    boost::algorithm::replace_all( cppname, "::", "__" );
    cppname.insert( 0, "FUNCTORS_" );
    return cppname;
  }
  std::unique_ptr<std::ostream> openFile( std::string namebase, unsigned short findex,
                                          std::vector<std::string> const& lines,
                                          std::vector<std::string> const& headers ) {
    // construct the file name
    //  1) remove trailing .cpp
    if ( boost::algorithm::ends_with( namebase, ".cpp" ) ) boost::algorithm::erase_tail( namebase, 4 );
    //  2) replace blanks  by underscore
    std::replace( namebase.begin(), namebase.end(), ' ', '_' );
    //  3) construct the name
    boost::format fname( "%s_%04d.cpp" );
    fname % namebase % findex;
    auto file = std::make_unique<std::ofstream>( fname.str() );
    // write the include directives
    *file << "// Generic statements\n";
    for ( const auto& l : lines ) { *file << l << '\n'; }
    *file << "// Explicitly declared include files:\n";
    for ( const auto& h : headers ) { include( *file, h ); }
    return file;
  }

  /** Write out the entry in a generated functor cache .cpp file for a single functor.
   */
  std::ostream& makeCode( std::ostream& stream, Functors::Cache::HashType hash, std::string_view type,
                          std::string_view code ) {
    boost::format declareFactory{R"_(namespace {
  using t_%1% = %2%;
  Gaudi::PluginService::DeclareFactory<t_%1%> functor_%1%{
     Functors::Cache::id( %1% ),
     []() {
       return std::make_unique<t_%1%>(
         %3%
       );
     }
  };
}
)_"};
    return stream << ( declareFactory % boost::io::group( std::showbase, std::hex, hash ) % type % code );
  }
} // namespace

/** @class FunctorFactory
 *
 *  This service does all the heavy lifting behind compiling functors into
 *  type-erased Functor<Out(In)> objects. It can do this either via ROOT's
 *  just-in-time compilation backend, or by using a pre-compiled functor
 *  cache. The tool is also responsible for generating the code that
 *  produces the precompiled functor cache. It is heavily inspired by Vanya
 *  Belyaev's LoKi::Hybrid::Base.
 */
struct FunctorFactory : public extends<Service, Functors::IFactory> {
  using extends::extends;

  // pointer-to-function type we use when JIT compiling
  using factory_function_ptr = functor_base_t ( * )();

  functor_base_t get_impl( Gaudi::Algorithm* owner, std::string_view functor_type,
                           ThOr::FunctorDesc const& desc ) override {
    // Prepare the string that fully specifies the functor we want to retrieve -- basically the combination of
    // input type, output type, functor string
    auto const& headers = desc.headers;
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Decoding " << desc.code << endmsg;
      debug() << "With extra headers:";
      for ( auto const& header_line : headers ) { debug() << " " << header_line; }
      debug() << endmsg;
    }

    // FIXME it seems that having a quoted string in the middle of the
    //       string adds quotes at the start/end...need Gaudi!919
    std::size_t findex{desc.code.front() == '"'}, codelen{desc.code.size() - findex - ( desc.code.back() == '"' )};
    auto        trimmed_code = std::string_view{desc.code}.substr( findex, codelen );

    // This is basically Functor<Out(In)>( PTCUT( ... ) ... )
    std::string full_code{functor_type};
    full_code.append( "( " );
    full_code.append( trimmed_code );
    full_code.append( " )" );

    // Now we can calculate the hash
    const auto hash = Functors::Cache::makeHash( full_code );

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << "Full string for hash: " << full_code << endmsg;
      verbose() << "Resulting hash is " << std::hex << std::showbase << hash << endmsg;
    }

    // The object we'll eventually return
    functor_base_t functor;

    // See if we can magically load the functor from the cache (Gaudi magic!)
    // Don't bother trying if we were told not to
    if ( this->m_use_cache ) {
      functor = ::Gaudi::PluginService::Factory<Functors::AnyFunctor*()>::create( Functors::Cache::id( hash ) );
      if ( !functor && !this->m_use_cling ) {
        // We print a different INFO message below if m_use_cling is true
        info() << "Cache miss for functor: " << trimmed_code << endmsg;
      }
    }

    if ( !functor && this->m_use_cling ) {
      // See if we already JIT compiled this functor and can therefore reuse
      // the factory function that we got before
      auto iter = m_factories.find( hash );
      if ( iter != m_factories.end() ) {
        // We had already JIT compiled this functor
        info() << "Reusing cling compiled factory for functor: " << trimmed_code << endmsg;
        auto factory_function = iter->second;
        functor               = factory_function();
      } else {
        // Need to actually do the JIT compilation
        if ( this->m_use_cache ) {
          info() << "Cache miss for functor: " << trimmed_code << ", now trying cling with headers " << headers
                 << endmsg;
        } else {
          info() << "Using cling for functor: " << trimmed_code << " with headers " << headers << endmsg;
        }

        // Shorthand for throwing an informative exception
        auto exception = [functor_type]( auto const& name ) {
          std::string ourname{"FunctorFactory::get<"};
          ourname.append( functor_type );
          ourname.append( ">" );
          return GaudiException{name, std::move( ourname ), StatusCode::FAILURE};
        };

        // The expression we ask cling to compile is not quite the same as
        // 'full_code'. Instead of Functor<Out(In)>( PT > ... ) we ask it to
        // compile the declaration of a function returning functor_base_t that
        // takes no arguments. We then ask cling to give us the address of
        // this function and call it ourselves. This looks like:
        // functor_base_t functor_0xdeadbeef() {
        //   return std::make_unique<Functor<Out(In)>>( PT > ... );
        // }
        std::ostringstream code;

        // Enable -O3-like code optimisation
        code << "#pragma cling optimize(3)\n";

        // Work around cling not auto-loading based on functions...?
        code << "#pragma cling load(\"TrackKernel\")\n";

        // Workaround for cling errors along the lines of:
        //   unimplemented pure virtual method 'i_cast' in 'RelationWeighted'
        // and several others. See https://gitlab.cern.ch/gaudi/Gaudi/issues/85
        // for discussion of a better solution.
#ifdef GAUDI_V20_COMPAT
        code << "#ifndef GAUDI_V20_COMPAT\n";
        code << "#define GAUDI_V20_COMPAT\n";
        code << "#endif\n";
#endif

        // Include the required headers
        for ( auto const& header : headers ) { include( code, header ); }

        // Get the name for the factory function. Maybe this should change so as
        // not to be the same as the cache entries, but for now it works fine.
        auto function_name = Functors::Cache::id( hash );

        // Declare the factory function
        code << functor_base_t_str << " " << function_name << "() { return std::make_unique<" << functor_type << ">( "
             << trimmed_code << " ); }\n";

        // Try and JIT compile this expression
        if ( !gInterpreter->Declare( code.str().c_str() ) ) {
          info() << "Code we attempted to JIT compile was:\n" << code.str() << endmsg;
          throw exception( "Failed to JIT compile functor" );
        }

        // Get the address of the factory function we just JITted
        TInterpreter::EErrorCode error_code{TInterpreter::kNoError};
        auto                     value = gInterpreter->Calc( function_name.c_str(), &error_code );
        if ( error_code != TInterpreter::kNoError ) {
          throw exception( "Failed to get the factory function address, error code " + std::to_string( error_code ) );
        }

        // Cast the function pointer to the correct type
        auto factory_function = reinterpret_cast<factory_function_ptr>( value );

        // Save the factory function pointer
        m_factories.emplace( hash, factory_function );

        // Use the JITted factory function
        functor = factory_function();
      }
    }

    if ( functor ) {
      functor->bind( owner );
    } else if ( this->m_use_cache || this->m_use_cling ) {
      // Don't emit too many messages while generating the functor caches. In
      // that case both cling and the cache are disabled, so we will never
      // actually retrieve a functor here.
      info() << "Functor could not be retrieved, it will not be bound" << endmsg;
    }

    // If we're going to write out the .cpp files for creating the functor cache when we finalise then we need to
    // store the relevant data in an internal structure
    if ( this->m_makeCpp ) {
      // Sort and de-duplicate the headers
      auto local_headers = headers;
      std::sort( local_headers.begin(), local_headers.end() );
      local_headers.erase( std::unique( local_headers.begin(), local_headers.end() ), local_headers.end() );
      // Store the functor alongside others with the same headers
      m_functors[std::move( local_headers )].emplace( hash, functor_type, trimmed_code );
    }

    return functor;
  }

  /** Write out the C++ files needed to compile the functor cache if needed.
   */
  StatusCode finalize() override {
    if ( m_makeCpp ) { writeCpp(); }
    return Service::finalize();
  }

protected:
  using HashType     = Functors::Cache::HashType;
  using FactoryCache = std::map<HashType, factory_function_ptr>;
  using FunctorSet   = std::map<std::vector<std::string>, std::set<std::tuple<HashType, std::string, std::string>>>;
  FunctorSet   m_functors;  // {headers: [{hash, type, code}, ...]}
  FactoryCache m_factories; // {hash: factory function pointer, ...}

  /** @brief Generate the functor cache .cpp files.
   *
   *  In order to expose as many bugs as possible, make sure that we generate a
   *  different .cpp file for every set of requested headers, so every functor
   *  is compiled with *exactly* the headers that were requested.
   */
  void writeCpp() const {
    /** The LoKi meaning of this parameter was:
     *  - positive: write N-files
     *  - negative: write N-functors per file
     *  - zero    : write one file
     *  currently it is not fully supported, we just use it to check that CMake
     *  is aware of at least as many source files as the minimum we need.
     *
     *  @todo When split > m_functors.size() then split the functors across
     *        more files until we are writing to all 'split' available source
     *        files.
     */
    std::size_t split{0};
    if ( !boost::conversion::try_lexical_convert( System::getEnv( "LOKI_GENERATE_CPPCODE" ), split ) ) { split = 0; }
    if ( m_functors.size() > split ) {
      throw GaudiException{"Functor factory needs to generate at least " + std::to_string( m_functors.size() ) +
                               " source files, but LOKI_GENERATE_CPPCODE was set to " + std::to_string( split ) +
                               ". Increase the SPLIT setting in the call to loki_functors_cache() to at least " +
                               std::to_string( m_functors.size() ),
                           "FunctorFactory", StatusCode::FAILURE};
    }

    /** We write one file for each unique set of headers
     */
    unsigned short ifile{0};
    for ( auto const& [headers, func_set] : m_functors ) {
      std::unique_ptr<std::ostream> file;
      unsigned short                iwrite{0};
      for ( auto const& [hash, functor_type, brief_code] : func_set ) {
        if ( !file ) { file = openFile( m_cppname, ++ifile, m_cpplines, headers ); }

        *file << '\n' << std::dec << std::noshowbase << "// FUNCTOR #" << ++iwrite << "/" << func_set.size() << '\n';

        // write actual C++ code
        ::makeCode( *file, hash, functor_type, brief_code );

        *file << '\n';
      }
    }
  }

  // Flags to steer the use of cling and the functor cache
  Gaudi::Property<bool> m_use_cache{this, "UseCache", System::getEnv( "LOKI_DISABLE_CACHE" ) == "UNKNOWN"};
  Gaudi::Property<bool> m_use_cling{this, "UseCling", System::getEnv( "LOKI_DISABLE_CLING" ) == "UNKNOWN"};
  Gaudi::Property<bool> m_makeCpp{this, "MakeCpp", System::getEnv( "LOKI_GENERATE_CPPCODE" ) != "UNKNOWN"};

  // Properties steering the generated functor cache code
  Gaudi::Property<std::string>              m_cppname{this, "CppFileName", make_default_cppname( this->name() )};
  Gaudi::Property<std::vector<std::string>> m_cpplines{this, "CppLines", {"#include \"Functors/Cache.h\""}};
};

DECLARE_COMPONENT( FunctorFactory )
