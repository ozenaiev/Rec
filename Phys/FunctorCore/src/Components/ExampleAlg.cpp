/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "GaudiAlg/FilterPredicate.h"

namespace {
  template <std::size_t... Is>
  std::array<std::pair<std::string, std::string>, sizeof...( Is )>
  __create_default_helper( std::index_sequence<Is...> ) {
    return {std::pair{"Input" + std::to_string( Is ), "Val" + std::to_string( Is )}...};
  }

  template <typename... Ins>
  using FilterPredicate = typename Gaudi::Functional::FilterPredicate<bool( Ins const&... )>;
} // namespace

template <typename... Ins>
struct FunctorExampleAlg;

// no arguments
template <>
struct FunctorExampleAlg<> final : public FilterPredicate<> {
  // This the type-erased wrapper around the functor we use to make the decision
  using Predicate = Functors::Functor<bool()>;

  using FilterPredicate::FilterPredicate;

  bool operator()() const override {
    auto p = m_pred.prepare();
    this->info() << m_functorproxy << endmsg;
    auto result = this->m_pred();
    this->info() << "Result: " << result;
    m_cutEff += result;
    return result;
  }

  StatusCode initialize() override {
    auto sc = FilterPredicate::initialize();
    decode();
    return sc;
  }

private:
  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Functor cut efficiency"};

  // all the information to tiate the functor via the factory m_factory
  Gaudi::Property<ThOr::FunctorDesc> m_functorproxy{
      this, "Cut", {"::Functors::AcceptAll{}", {"Functors/Function.h"}, "ALL"}, [this]( auto& ) {
        if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
        this->decode();
      }};

  // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
  // I think this has some overhead, but it may be avoidable...TBC
  Predicate m_pred;

  // This is the factory tool that handles the functor cache and/or the just-in-time compilation
  ServiceHandle<Functors::IFactory> m_factory{this, "Factory", "FunctorFactory"};

  void decode() {
    m_factory.retrieve().ignore();
    m_pred = m_factory->get<Predicate>( this, m_functorproxy );
  }
};

// with arguments
template <typename... Ins>
struct FunctorExampleAlg final : public FilterPredicate<Ins...> {
  // This the type-erased wrapper around the functor we use to make the decision
  using Predicate = Functors::Functor<bool( Ins... )>;

  using base = FilterPredicate<Ins...>;

  FunctorExampleAlg( std::string const& name, ISvcLocator* pSvcLocator )
      : base::FilterPredicate( name, pSvcLocator, __create_default_helper( std::index_sequence_for<Ins...>{} ) ) {}

  bool operator()( Ins const&... ins ) const override {
    auto p = m_pred.prepare();
    this->info() << m_functorproxy << endmsg;
    auto result = p( ins... );
    this->info() << "Result: " << result;
    m_cutEff += result;
    return result;
  }

  StatusCode initialize() override {
    auto sc = FilterPredicate<Ins...>::initialize();
    decode();
    return sc;
  }

private:
  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Functor cut efficiency"};

  // all the information to instantiate the functor via the factory m_factory
  Gaudi::Property<ThOr::FunctorDesc> m_functorproxy{
      this, "Cut", {"::Functors::AcceptAll{}", {"Functors/Function.h"}, "ALL"}, [this]( auto& ) {
        if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
        this->decode();
      }};

  // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
  // I think this has some overhead, but it may be avoidable...TBC
  Predicate m_pred;

  // This is the factory tool that handles the functor cache and/or the just-in-time compilation
  ServiceHandle<Functors::IFactory> m_factory{this, "Factory", "FunctorFactory"};

  void decode() {
    m_factory.retrieve().ignore();
    m_pred = m_factory->get<Predicate>( this, m_functorproxy );
  }
};
DECLARE_COMPONENT_WITH_ID( FunctorExampleAlg<>, "FunctorExampleAlg" )
DECLARE_COMPONENT_WITH_ID( FunctorExampleAlg<int>, "FunctorExampleAlg_int" )
// using fea_if = FunctorExampleAlg<int,float>;
// using fea_vi = FunctorExampleAlg<std::vector<int>>;
// DECLARE_COMPONENT_WITH_ID( fea_if, "FunctorExampleAlg_int_float" )
// DECLARE_COMPONENT_WITH_ID( fea_vi, "FunctorExampleAlg_vector_int" )
