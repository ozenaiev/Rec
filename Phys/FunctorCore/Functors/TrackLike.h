/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/detected.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"

/** @file  TrackLike.h
 *  @brief Definitions of functors for track-like objects.
 */

/** @namespace Functors::Track
 *
 *  This defines some basic functors that operate on track-like, or maybe more
 *  accurately charged-particle-like, objects. Both predicates and functions
 *  are defined in the same file for the moment.
 */
namespace Functors::Track {
  /** @brief Momentum, as defined by the p() accessor.
   */
  struct Momentum : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.p();
    }
  };

  /** @brief Azimuthal angle, as defined by the phi() accessor.
   */
  struct Phi : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.phi();
    }
  };

  /** @brief Transverse momentum, as defined by the pt() accessor.
   */
  struct TransverseMomentum : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.pt();
    }
  };

  /** @brief IsMuon, as defined by the accessor of the same name.
   */
  struct IsMuon : public Predicate {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.IsMuon();
    }
  };

  /** @brief eta/pseudorapidity, as defined by the pseudoRapidity accessor.
   */
  struct PseudoRapidity : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.pseudoRapidity();
    }
  };

  /** @brief chi^2/d.o.f., as defined by the chi2PerDoF accessor.
   */
  struct Chi2PerDoF : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.chi2PerDoF();
    }
  };

  /** @brief Ghost probability, as defined by the ghostProbability accessor.
   */
  struct GhostProbability : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.ghostProbability();
    }
  };
  /** @brief Z of the ClosestToBeam State, as defined by the closestToBeamZ accessor.
   */
  struct ClosestToBeamZ : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.closestToBeamState().position().Z();
    }
  };

  /** @brief nHits, as defined by the nHits accessor.
   */
  struct nHits : public Function {
    static constexpr auto name() { return "nHits"; }
    template <typename Data>
    auto operator()( Data const& track ) const {
      return track.nHits();
    }
  };
} // namespace Functors::Track

namespace Functors::detail {
  template <typename StatePos_t, typename StateDir_t, typename VertexPos_t>
  auto impactParameterSquared( StatePos_t const& state_pos, StateDir_t const& state_dir, VertexPos_t const& vertex ) {
    return ( state_pos - vertex ).Cross( state_dir ).mag2() / state_dir.mag2();
  }

  template <typename T>
  using has_loop_mask_ = decltype( std::declval<T>().loop_mask() );

  template <typename T>
  inline constexpr bool has_loop_mask_v = Gaudi::cpp17::is_detected_v<has_loop_mask_, T>;

  template <typename F>
  inline constexpr auto loop_mask( F const& track_chunk ) {
    if constexpr ( has_loop_mask_v<F> ) {
      return track_chunk.loop_mask();
    } else {
      return true;
    }
  }

  template <typename VContainer>
  struct MinimumImpactParameter {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "MinimumImpactParameter"; }

    template <typename TrackChunk>
    auto operator()( VContainer const& vertices, TrackChunk const& track_chunk ) const {
      auto&& state     = track_chunk.closestToBeamState();
      auto&& state_pos = state.position();
      auto&& state_dir = state.slopes();
      using std::min;  // so min() below works when float_v is plain float or double
      using std::sqrt; // ...
      using vector_t = std::decay_t<decltype( state_pos )>;
      using float_v  = decltype( state_pos.X() );
      auto min_ip2 =
          std::accumulate( std::begin( vertices ), std::end( vertices ), float_v{std::numeric_limits<float>::max()},
                           [&]( float_v ip2, auto const& vertex ) {
                             auto const& PV = vertex.position();
                             auto        this_ip2 =
                                 impactParameterSquared( state_pos, state_dir, vector_t( PV.x(), PV.y(), PV.z() ) );
                             static_assert( std::is_same_v<decltype( this_ip2 ), float_v> );
                             return min( ip2, this_ip2 );
                           } );
      return sqrt( min_ip2 );
    }
  };

  template <typename VContainer>
  struct MinimumImpactParameterChi2 {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "MinimumImpactParameterChi2"; }

    template <typename TrackChunk>
    auto operator()( VContainer const& vertices, TrackChunk const& track_chunk ) const {
      // Make sure min() works for plain floating point types
      using std::min;
      // Accept generic containers
      using std::begin;
      using std::end;
      // Figure out the return type (probably float or SIMDWrapper's float_v for now)
      using float_v = decltype( Sel::Utils::impactParameterChi2( track_chunk, vertices.front() ) );
      // FIXME SIMDWrapper should specialise std::numeric_limits for the types
      //       it defines so we can use std::numeric_limits<float_v>::max().
      //       Right now that is VERY dangerous, as it returns float_v(), which
      //       is an undefined value.
      return std::accumulate( begin( vertices ), end( vertices ), float_v{std::numeric_limits<float>::max()},
                              [&track_chunk]( float_v ipchi2, auto const& vertex ) {
                                return min( ipchi2, Sel::Utils::impactParameterChi2( track_chunk, vertex ) );
                              } );
    }
  };

  template <typename VContainer>
  struct ImpactParameterChi2ToVertex {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "ImpactParameterChi2ToVertex"; }

    template <typename Particle>
    auto operator()( VContainer const& vertices, Particle const& particle ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      return Sel::getBestPVRel( particle, vertices ).ipchi2();
    }
  };

  template <typename VContainer>
  struct MinimumImpactParameterCut {
    MinimumImpactParameterCut( float cut_value ) : m_cut_value_squared( std::pow( cut_value, 2 ) ) {}

    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "MinimumImpactParameterCut"; }

    template <typename TrackChunk>
    auto operator()( VContainer const& vertices, TrackChunk const& track_chunk ) const {
      auto&& state     = track_chunk.closestToBeamState();
      auto&& state_pos = state.position();
      auto&& state_dir = state.slopes();
      using Sel::Utils::none; // so none() below works when mask_v is plain bool
      using std::min;         // so min() below works when float_v is plain float or double
      using vector_t = std::decay_t<decltype( state_pos )>;
      using float_v  = decltype( state_pos.X() );
      using mask_v   = decltype( state_pos.X() > m_cut_value_squared );
      mask_v  mask{float_v{1.f} > float_v{0.f}}; // FIXME so we can put 'true'...
      mask_v  loop_mask{detail::loop_mask( track_chunk )};
      float_v min_d{std::numeric_limits<float>::max()};
      for ( auto const& vertex : vertices ) {
        auto const& PV = vertex.position();
        min_d =
            min( min_d, detail::impactParameterSquared( state_pos, state_dir, vector_t( PV.x(), PV.y(), PV.z() ) ) );
        mask = min_d > m_cut_value_squared;
        if ( none( mask && loop_mask ) ) {
          // Not completely obvious the overhead of short-circuiting is a net
          // gain for the vectorised version, but let's do it anyway for now.
          break;
        }
      }
      return mask;
    }

  private:
    float m_cut_value_squared = 0.f;
  };

  template <typename VContainer>
  struct MinimumImpactParameterChi2Cut {
    MinimumImpactParameterChi2Cut( float cut_value ) : m_cut_value{cut_value} {}

    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "MinimumImpactParameterChi2Cut"; }

    template <typename TrackChunk>
    auto operator()( VContainer const& vertices, TrackChunk const& track_chunk ) const {
      // Check how much of 'track_chunk' we should operate on
      auto loop_mask = detail::loop_mask( track_chunk );
      // Figure out what the types are. It would be nice if we could retire this part...
      using mask_v  = decltype( loop_mask ); // e.g. bool, SIMDWrapper mask_v, ...
      using float_v = decltype( Sel::Utils::impactParameterChi2( track_chunk, vertices.front() ) );
      // FIXME modify SIMDWrapper's mask_v types to take bool initialisers
      mask_v mask{float_v{1.f} > float_v{0.f}}; // i.e. 'true', return this if there are no vertices
      // Keep track of the smallest ipchi2 as we loop, abort if they're all below the cut value
      // FIXME avoid writing 'float' here
      float_v min_ipchi2{std::numeric_limits<float>::max()};
      // Make sure min() works for basic arithmetic types
      using std::min;
      // Make sure none() works for plain bool
      using Sel::Utils::none;
      // Unfortunately it seems difficult to avoid the explicit loop...
      for ( auto const& vertex : vertices ) {
        min_ipchi2 = min( min_ipchi2, Sel::Utils::impactParameterChi2( track_chunk, vertex ) );
        mask       = min_ipchi2 > m_cut_value;
        // Check if all tracks in the chunk have had an ipchi2 below threshold
        // w.r.t. one of the vertices we've already looped over. If yes, we can
        // safely abort the loop over vertices
        if ( none( loop_mask && mask ) ) { break; }
      }
      return mask;
    }

  private:
    float m_cut_value = 0.f;
  };
} // namespace Functors::detail

namespace Functors::Track {
  /** @brief Minimum impact parameter w.r.t. any of the given vertices.
   *
   *  Note that for the typical MINIP > x cut it is more efficient to use the
   *  dedicated functor that can abort the loop over vertices early.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameter( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::MinimumImpactParameter<VContainer>, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Minimum impact parameter chi2 w.r.t. any of the given vertices.
   *
   *  Note that for the typical MINIPCHI2 > x cut it is more efficiency to use
   *  the dedicated functor that can abort the loop over vertices early.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameterChi2( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::MinimumImpactParameterChi2<VContainer>, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Impact parameter chi2 to the "best" one of the given vertices.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used. The link caches the ipchi2 value, so this should be the
   *  most efficient way of getting the ipchi2 value.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto ImpactParameterChi2ToVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::ImpactParameterChi2ToVertex<VContainer>, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Require the minimum impact parameter w.r.t. any of the given
   *         vertices is greater than some threshold.
   *
   *  @param cut_value    Minimum impact parameter value.
   *  @param tes_location TES location of vertex container.
   *
   *  Note this is more efficient than computing the minimum and then cutting
   *  on it, as this functor can short circuit.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameterCut( float cut_value, std::string vertex_location ) {
    return detail::add_data_deps<Predicate, VContainer>( detail::MinimumImpactParameterCut<VContainer>{cut_value},
                                                         std::move( vertex_location ) );
  }

  /** @brief Require the minimum impact parameter chi2 w.r.t. any of the given
   *         vertices is greater than some threshold.
   *
   *  @param cut_value    Minimum impact parameter chi2 value.
   *  @param tes_location TES location of vertex container.
   *
   *  Note this is more efficient than computing the minimum and then cutting
   *  on it, as this functor can short circuit.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameterChi2Cut( float cut_value, std::string vertex_location ) {
    return detail::add_data_deps<Predicate, VContainer>( detail::MinimumImpactParameterChi2Cut<VContainer>{cut_value},
                                                         std::move( vertex_location ) );
  }
} // namespace Functors::Track
