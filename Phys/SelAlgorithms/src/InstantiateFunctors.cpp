/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/PrIterableForwardTracks.h"
#include "Event/PrIterableVeloTracks.h"
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "Functors/Filter.h"
#include "Functors/Function.h"
#include "Functors/with_functor_maps.h"
#include "Functors/with_functors.h"
#include "Kernel/HeaderMapping.h"
#include "PrKernel/PrSelection.h"
#include "SelKernel/TrackCombination.h"
#include "SelKernel/TrackZips.h"
#include "TrackKernel/TrackCompactVertex.h"

#include "GaudiAlg/Consumer.h"

#include <any>
#include <vector>

namespace {
  // Functor that filters container->container
  template <typename T>
  struct Cut {
    constexpr static auto PropertyName = "Cut";
    constexpr static auto ExtraHeaders = LHCb::header_map_v<T>;
    using Signature                    = Functors::filtered_t<T>( T const& );
  };

  // Default -- get the value_type of the actual type
  template <typename T, typename U = void>
  struct value_type {
    using type = std::decay_t<typename T::value_type>;
  };

  // Zippable -- get the value_type of the iterable zip instead
  template <typename T>
  struct value_type<T, std::enable_if_t<LHCb::Pr::is_zippable_v<T>>> {
    using type = std::decay_t<typename LHCb::Pr::zip_t<T>::value_type>;
  };

  // Functor that acts on "an element of" a container
  template <typename T>
  struct Func {
    constexpr static auto PropertyName = "Functions";
    constexpr static auto ExtraHeaders = LHCb::header_map_v<T>;
    using Signature                    = std::any( typename value_type<T>::type const& );
  };

  struct VoidFunc {
    constexpr static auto PropertyName = "Functions";
    using Signature                    = std::any();
  };

  template <typename T>
  using base_t = with_functors<with_functor_maps<Gaudi::Functional::Consumer<void()>, Func<T>>, Cut<T>>;

  using void_base_t = with_functor_maps<Gaudi::Functional::Consumer<void()>, VoidFunc>;
} // namespace

template <typename T>
struct InstantiateFunctors final : public base_t<T> {
  using base_t<T>::base_t;
  using base_t<T>::info;
  using base_t<T>::debug;
  using base_t<T>::msgLevel;
  void operator()() const override {}

  /** Print out the (erased) return types of the various functors
   */
  StatusCode initialize() override {
    auto        sc  = base_t<T>::initialize();
    auto const& cut = this->template getFunctor<Cut<T>>();
    if ( cut ) { info() << "Cut return type is " << System::typeinfoName( cut.rtype() ) << endmsg; }
    auto const& map = this->template getFunctorMap<Func<T>>();
    for ( auto const& [nickname, func] : map ) {
      if ( func ) {
        info() << "Function " << nickname << " return type is " << System::typeinfoName( func.rtype() ) << endmsg;
      }
    }
    return sc;
  }
};

/** TODO: make this share some functor-checking code with above
 */
struct InstantiateVoidFunctors final : public void_base_t {
  using void_base_t::debug;
  using void_base_t::info;
  using void_base_t::msgLevel;
  using void_base_t::void_base_t;
  void operator()() const override {}

  /** Print out the (erased) return types of the various functors
   */
  StatusCode initialize() override {
    auto        sc  = void_base_t::initialize();
    auto const& map = this->template getFunctorMap<VoidFunc>();
    for ( auto const& [nickname, func] : map ) {
      if ( func ) {
        info() << "Function " << nickname << " return type is " << System::typeinfoName( func.rtype() ) << endmsg;
      }
    }
    return sc;
  }
};

DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<Pr::Selection<LHCb::Event::v1::Track>>, "InstantiateFunctors__Track_v1" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<Pr::Selection<LHCb::Event::v2::Track>>, "InstantiateFunctors__Track_v2" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Velo::Tracks>, "InstantiateFunctors__PrVeloTracks" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Iterable::Scalar::Velo::Tracks>,
                           "InstantiateFunctors__PrVeloTracks__Unwrapped" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Forward::Tracks>, "InstantiateFunctors__PrForwardTracks" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Iterable::Scalar::Forward::Tracks>,
                           "InstantiateFunctors__PrForwardTracks__Unwrapped" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Fitted::Forward::Tracks>,
                           "InstantiateFunctors__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Iterable::Scalar::Fitted::Forward::Tracks>,
                           "InstantiateFunctors__PrFittedForwardTracks__Unwrapped" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Fitted::Forward::TracksWithPVs>,
                           "InstantiateFunctors__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs>,
                           "InstantiateFunctors__PrFittedForwardTracksWithPVs__Unwrapped" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                           "InstantiateFunctors__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithMuonID>,
                           "InstantiateFunctors__PrFittedForwardTracksWithMuonID__Unwrapped" )
using vector__TrackCompactVertex__2_double = std::vector<LHCb::TrackKernel::TrackCompactVertex<2, double>>;
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<vector__TrackCompactVertex__2_double>,
                           "InstantiateFunctors__vector__TrackCompactVertex__2_double" )
using TrackCombination__ScalarFittedWithMuonID__2 =
    TrackCombination<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithMuonID::value_type, 2>;
using vector__TrackCombination__ScalarFittedWithMuonID__2 = std::vector<TrackCombination__ScalarFittedWithMuonID__2>;
DECLARE_COMPONENT_WITH_ID( InstantiateFunctors<vector__TrackCombination__ScalarFittedWithMuonID__2>,
                           "InstantiateFunctors__vector__TrackCombination__ScalarFittedWithMuonID__2" )
DECLARE_COMPONENT_WITH_ID( InstantiateVoidFunctors, "InstantiateFunctors__void" )