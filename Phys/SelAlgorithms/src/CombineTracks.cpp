/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/Track_v2.h"
#include "Functors/Function.h"
#include "Functors/IFactory.h"
#include "Functors/with_functor_maps.h"
#include "Functors/with_functors.h"
#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrSelection.h"
#include "SelKernel/TrackCombination.h"
#include "SelKernel/TrackZips.h"
#include "TrackKernel/TrackCompactVertex.h"
#include "TrackKernel/TrackNProngVertex.h"
#include <TFile.h>
#include <TTree.h>
#include <tuple>

namespace {
  using FPtype = double;

  template <std::size_t N>
  using OutputStorage = std::vector<LHCb::TrackKernel::TrackCompactVertex<N, FPtype>>;

  // Shorthand for below.
  template <typename T, std::size_t N>
  using FilterTransform =
      Gaudi::Functional::MultiTransformerFilter<std::tuple</* OutputView,*/ OutputStorage<N>>( T const& )>;

  template <typename T, std::size_t N>
  struct CombTypes {
    using TrackType               = typename T::value_type;
    using Combination             = TrackCombination<TrackType, N>;
    constexpr static auto Headers = LHCb::header_map_v<T> + "SelKernel/TrackCombination.h";
    struct Cut {
      using Signature                    = bool( Combination const& );
      constexpr static auto PropertyName = "CombinationCut";
      constexpr static auto ExtraHeaders = Headers;
    };
    struct Dump {
      using Signature                    = std::any( Combination const& );
      constexpr static auto PropertyName = "CombinationDump";
      constexpr static auto ExtraHeaders = Headers;
    };
  };

  template <typename T, std::size_t N>
  struct VertexTypes {
    using VertexType              = typename OutputStorage<N>::value_type;
    constexpr static auto Headers = {"TrackKernel/TrackCompactVertex.h"};
    struct Cut {
      using Signature                    = bool( VertexType const& );
      constexpr static auto PropertyName = "VertexCut";
      constexpr static auto ExtraHeaders = Headers;
    };
    struct Dump {
      using Signature                    = std::any( VertexType const& );
      constexpr static auto PropertyName = "VertexDump";
      constexpr static auto ExtraHeaders = Headers;
    };
  };

  struct VoidDump {
    using Signature                    = std::any();
    constexpr static auto PropertyName = "VoidDump";
  };

  template <typename T, std::size_t N>
  struct ChildDump {
    using TrackType                    = typename T::value_type;
    using Signature                    = std::any( TrackType const& );
    constexpr static auto PropertyName = "ChildDump";
    constexpr static auto ExtraHeaders = LHCb::header_map_v<T>;
  };

  template <typename T, std::size_t N>
  using nodump_base_t =
      with_functors<FilterTransform<T, N>, typename CombTypes<T, N>::Cut, typename VertexTypes<T, N>::Cut>;

  template <typename T, std::size_t N>
  using dump_base_t = with_functor_maps<nodump_base_t<T, N>, typename CombTypes<T, N>::Dump,
                                        typename VertexTypes<T, N>::Dump, ChildDump<T, N>, VoidDump>;

  template <typename T, std::size_t N, bool EnableDumping>
  using base_t = std::conditional_t<EnableDumping, dump_base_t<T, N>, nodump_base_t<T, N>>;

  template <typename F, typename... RTypes>
  class GenericTreeHelper {
    static_assert( sizeof...( RTypes ) > 0, "GenericTreeHelper was not given any possible data types" );
    template <typename T>
    bool help( TTree* tree, std::type_info const& rtype, std::string const& branchname ) {
      if ( rtype != typeid( T ) ) { return false; }
      *m_value = T{};
      tree->Branch( branchname.c_str(), &std::get<T>( *m_value ) );
      return true;
    }

  public:
    GenericTreeHelper( TTree* tree, std::string const& branchname, F const& functor ) : m_f{functor} {
      auto const& rtype = functor.rtype();
      if ( ( help<RTypes>( tree, rtype, branchname ) || ... ) ) return;
      throw std::runtime_error( "GenericTreeHelper did not recognise return type: " + System::typeinfoName( rtype ) );
    }

    template <typename... Args>
    void operator()( Args&&... args ) {
      if ( !m_value ) { throw std::runtime_error( "..." ); }
      // Evaluate the functor
      std::any fval = m_f( std::forward<Args>( args )... );
      // Sanity check
      auto old_index = m_value->index();
      // *m_value should already hold the same return type as that hidden by
      // the std::any functor return type, thanks to the magic of rtype(), so
      // we should be able to pass this type directly to std::any_cast.
      std::visit( [&]( auto& x ) { *m_value = std::any_cast<std::decay_t<decltype( x )>>( fval ); }, *m_value );
      // Sanity check -- remove this..
      if ( UNLIKELY( m_value->index() != old_index ) ) {
        throw std::runtime_error( "index changed when it shouldn't have done" );
      }
    }

  private:
    F const& m_f;
    using numeric_value = std::variant<RTypes...>;
    std::unique_ptr<numeric_value> m_value{std::make_unique<numeric_value>()};
  };

  template <class T>
  using TreeHelper = GenericTreeHelper<T, float, double, unsigned int, unsigned long long>;
} // namespace

/** @class CombineTracks PrCombineTracks.cpp
 *
 *  @tparam T TODO
 */
template <typename T, std::size_t N, bool EnableDumping = false>
class CombineTracks final : public base_t<T, N, EnableDumping> {
public:
  static_assert( N == 2 ); // FIXME not immediately implementing the logic for >2-body combinatorics
  using KeyValue      = typename base_t<T, N, EnableDumping>::KeyValue;
  using TrackType     = typename T::value_type;
  using StateType     = typename std::decay_t<decltype( std::declval<TrackType>().closestToBeamState() )>;
  using Combination   = typename CombTypes<T, N>::Combination;
  using VertexFitType = LHCb::TrackKernel::TrackNProngVertex<N, FPtype, StateType>;

  CombineTracks( const std::string& name, ISvcLocator* pSvcLocator )
      : base_t<T, N, EnableDumping>( name, pSvcLocator, {"InputTracks", ""},
                                     {KeyValue{"OutputVertices", ""} /* , KeyValue{"OutputVerticesStorage", ""}*/} ) {}

  std::tuple<bool, /*OutputView, */ OutputStorage<N>> operator()( T const& in ) const override {
    // Prepare the output storage
    OutputStorage<N> storage;

    // Do some potentially-expensive preparation before we enter the loop
    auto vertex_cut      = this->template getFunctor<typename VertexTypes<T, N>::Cut>().prepare( /* event_context */ );
    auto combination_cut = this->template getFunctor<typename CombTypes<T, N>::Cut>().prepare( /* event_context */ );

    // Buffer our counters to reduce the number of atomic operations
    auto npassed_vertex_cut      = m_npassed_vertex_cut.buffer();
    auto npassed_vertex_fit      = m_npassed_vertex_fit.buffer();
    auto npassed_combination_cut = m_npassed_combination_cut.buffer();

    if constexpr ( EnableDumping ) {
      // These are, by definition, independent of the candidate/combination,
      // so we can populate them now. This also means there's no reason to
      // bother with the explicit preparation step.
      for ( auto& filler : m_void_dump_functors ) { filler(); }
    }

    // TODO: replace with N-dependent combination generation...
    for ( auto t1_iter = in.begin(); t1_iter != in.end(); ++t1_iter ) {
      auto const& t1 = *t1_iter;
      for ( auto t2_iter = std::next( t1_iter ); t2_iter != in.end(); ++t2_iter ) {
        auto const& t2 = *t2_iter;
        Combination combination{{t1, t2}};
        auto        pass_combination_cut = combination_cut( combination );
        npassed_combination_cut += pass_combination_cut;
        if ( !pass_combination_cut ) { continue; }

        // Get the states -- this is a bit horrible for the moment
        auto const& s1 = t1.closestToBeamState();
        auto const& s2 = t2.closestToBeamState();

        // Make a new vertex
        VertexFitType vertex_fit{s1, s2};
        auto          fit_succeeded = ( vertex_fit.fit() == VertexFitType::FitSuccess );
        npassed_vertex_fit += fit_succeeded;
        if ( !fit_succeeded ) { continue; }

        // Convert it to the output vertex type
        // TODO: is this really needed? could we cut on vertex_fit and convert afterwards?
        auto& vertex = storage.emplace_back( convertToCompactVertex( vertex_fit ) );

        // Hackily add the child relations
        vertex.setChildZipIdentifier( 0, in.zipIdentifier() );
        vertex.setChildZipIdentifier( 1, in.zipIdentifier() );
        vertex.setChildIndex( 0, std::distance( in.begin(), t1_iter ) );
        vertex.setChildIndex( 1, std::distance( in.begin(), t2_iter ) );

        // Apply the vertex cut
        auto pass_vertex_cut = vertex_cut( vertex );
        npassed_vertex_cut += pass_vertex_cut;
        if ( !pass_vertex_cut ) {
          storage.pop_back();
          continue;
        }

        if constexpr ( EnableDumping ) {
          // For simplicity we only dump the children/combination/vertex
          // quantities after the vertex fit has passed. There is, therefore,
          // an implicit requirement that the vertex fit passed, and the
          // distributions are biased by this requirement + "later" cuts that
          // you choose to apply. But this way the relationship between
          // child/combination/parent in the output TTree is trivial, and if
          // you want to reduce the size of the output tuple by applying cuts
          // then that's easy too.
          for ( auto nchild = 0ul; nchild < N; ++nchild ) {
            for ( auto& filler : m_child_dump_functors[nchild] ) { filler( combination[nchild] ); }
          }
          for ( auto& filler : m_combination_dump_functors ) { filler( combination ); }
          for ( auto& filler : m_vertex_dump_functors ) { filler( vertex ); }
          m_root_tree->Fill();
        }
      }
    }

    // OutputView view; // TODO make this a useful N-independent view into 'storage'
    auto filter_pass = !storage.empty();
    return {filter_pass, /* std::move( view ), */ std::move( storage )};
  }

  StatusCode initialize() override {
    auto sc = base_t<T, N, EnableDumping>::initialize();
    if constexpr ( EnableDumping ) { set_up_dumping(); }
    return sc;
  }

  StatusCode finalize() override {
    if constexpr ( EnableDumping ) { cleanup_dumping(); }
    return base_t<T, N, EnableDumping>::finalize();
  }

private:
  /// Number of combinations passing the combination cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_combination_cut{this, "# passed combination cut"};
  /// Number of successfully fitted combinations
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_fit{this, "# passed vertex fit"};
  /// Number of fitted vertices passing vertex cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_cut{this, "# passed vertex cut"};

  /// Member variables relating to the dumping functionality. If only I could use if constexpr here...
  mutable TTree*         m_root_tree{nullptr};
  std::unique_ptr<TFile> m_root_file;
  mutable std::array<std::vector<TreeHelper<Functors::Functor<typename ChildDump<T, N>::Signature>>>, 2>
                                                                          m_child_dump_functors;
  mutable std::vector<TreeHelper<Functors::Functor<VoidDump::Signature>>> m_void_dump_functors;
  mutable std::vector<TreeHelper<Functors::Functor<typename CombTypes<T, N>::Dump::Signature>>>
      m_combination_dump_functors;
  mutable std::vector<TreeHelper<Functors::Functor<typename VertexTypes<T, N>::Dump::Signature>>>
                               m_vertex_dump_functors;
  Gaudi::Property<std::string> m_root_file_name{this, "DumpFileName", "PrCombineTracks.root"};

  template <typename Functor, typename Filler>
  void setup_branches( std::string_view prefix, std::map<std::string, Functor> const& functors,
                       std::vector<Filler>& fillers ) {
    fillers.clear();
    for ( auto const& [nickname, functor] : functors ) {
      fillers.emplace_back( m_root_tree, std::string{prefix} + nickname, functor );
    }
  }

  void set_up_dumping() {
    m_root_file.reset( TFile::Open( m_root_file_name.value().c_str(), "recreate" ) );
    m_root_file->cd();
    // m_root_tree gets managed by m_root_file, this isn't a dangling pointer
    m_root_tree = new TTree( "CombinerTree", "" );
    setup_branches( "", this->template getFunctorMap<VoidDump>(), m_void_dump_functors );
    setup_branches( "vertex_", this->template getFunctorMap<typename VertexTypes<T, N>::Dump>(),
                    m_vertex_dump_functors );
    setup_branches( "comb_", this->template getFunctorMap<typename CombTypes<T, N>::Dump>(),
                    m_combination_dump_functors );
    for ( auto i = 0ul; i < N; ++i ) {
      setup_branches( "child" + std::to_string( i ) + "_", this->template getFunctorMap<ChildDump<T, N>>(),
                      m_child_dump_functors[i] );
    }
  }

  void cleanup_dumping() {
    m_root_file->Write();
    m_root_file->Close();
  }
};

using CombineTracks__2Body__Track_v2         = CombineTracks<Pr::Selection<LHCb::Event::v2::Track>, 2>;
using CombineTracks__2Body__Track_v2__Dumper = CombineTracks<Pr::Selection<LHCb::Event::v2::Track>, 2, true>;
using CombineTracks__2Body__PrFittedForwardTracks =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::Tracks, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithPVs =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs, 2, true>;
using CombineTracks__2Body__PrFittedForwardTracksWithMuonID =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithMuonID, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithMuonID, 2, true>;
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__Track_v2, "CombineTracks__2Body__Track_v2" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__Track_v2__Dumper, "CombineTracks__2Body__Track_v2__Dumper" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracks, "CombineTracks__2Body__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithPVs,
                           "CombineTracks__2Body__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper,
                           "CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithMuonID,
                           "CombineTracks__2Body__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper,
                           "CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper" )