/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/HeaderMapping.h"
#include "TrackKernel/TrackVertexUtils.h"
#include <array>
#include <functional>

template <typename Track, std::size_t N>
struct TrackCombination {
  // If the 'Track' type defines contains 'is_proxy', and it's true, then store
  // it by value in the combination object. Otherwise, store reference_wrapper.
  template <typename T, typename = bool>
  struct is_proxy : std::false_type {};
  template <typename T>
  struct is_proxy<T, decltype( T::is_proxy == true )> : std::true_type {};
  using value_type  = std::conditional_t<is_proxy<Track>::value, Track, std::reference_wrapper<Track const>>;
  using child_array = std::array<value_type, N>;
  using state_t     = std::decay_t<decltype( std::declval<Track>().closestToBeamState() )>;
  child_array m_children;
  TrackCombination( child_array children ) : m_children( std::move( children ) ) {}
  constexpr Track const& operator[]( std::size_t pos ) const { return m_children[pos]; }

  auto momentum() const {
    return std::accumulate( m_children.begin(), m_children.end(), decltype( std::declval<Track>().momentum() )(),
                            []( auto sum, Track const& child ) { return sum + child.momentum(); } );
  }

  auto pt() const { return momentum().Rho(); }

  auto maxdoca() const {
    using doca_t = decltype( LHCb::TrackVertexUtils::doca( std::declval<state_t>(), std::declval<state_t>() ) );
    using std::max;
    using std::numeric_limits;
    doca_t maxdoca{numeric_limits<doca_t>::min()};
    for ( auto i1 = m_children.begin(); i1 != m_children.end(); ++i1 ) {
      Track const& t1 = *i1;
      auto const&  s1 = t1.closestToBeamState();
      for ( auto i2 = std::next( i1 ); i2 != m_children.end(); ++i2 ) {
        Track const& t2 = *i2;
        auto const&  s2 = t2.closestToBeamState();
        maxdoca         = max( maxdoca, LHCb::TrackVertexUtils::doca( s1, s2 ) );
      }
    }
    return maxdoca;
  }

  auto maxdocachi2() const {
    using docachi2_t =
        decltype( LHCb::TrackVertexUtils::vertexChi2( std::declval<state_t>(), std::declval<state_t>() ) );
    using std::max;
    using std::numeric_limits;
    docachi2_t maxdocachi2{numeric_limits<docachi2_t>::min()};
    for ( auto i1 = m_children.begin(); i1 != m_children.end(); ++i1 ) {
      Track const& t1 = *i1;
      auto const&  s1 = t1.closestToBeamState();
      for ( auto i2 = std::next( i1 ); i2 != m_children.end(); ++i2 ) {
        Track const& t2 = *i2;
        auto const&  s2 = t2.closestToBeamState();
        maxdocachi2     = max( maxdocachi2, LHCb::TrackVertexUtils::vertexChi2( s1, s2 ) );
      }
    }
    return maxdocachi2;
  }

  auto maxdocachi2cut( float cut ) const {
    for ( auto i1 = m_children.begin(); i1 != m_children.end(); ++i1 ) {
      Track const& t1 = *i1;
      auto const&  s1 = t1.closestToBeamState();
      for ( auto i2 = std::next( i1 ); i2 != m_children.end(); ++i2 ) {
        Track const& t2 = *i2;
        auto const&  s2 = t2.closestToBeamState();
        if ( LHCb::TrackVertexUtils::vertexChi2( s1, s2 ) > cut ) { return false; }
      }
    }
    return true;
  }
};

template <typename Track, std::size_t N>
auto begin( TrackCombination<Track, N> const& combination ) {
  return combination.m_children.begin();
}

template <typename Track, std::size_t N>
auto end( TrackCombination<Track, N> const& combination ) {
  return combination.m_children.end();
}

template <typename Track, std::size_t N>
struct LHCb::header_map<TrackCombination<Track, N>> {
  static constexpr auto value = LHCb::header_map_v<Track> + "SelKernel/TrackCombination.h";
};