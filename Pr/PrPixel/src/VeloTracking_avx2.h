/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// @Author: Arthur Hennequin (LIP6, CERN)

#pragma once

//#include "VeloClusterTracking.h"
#include <immintrin.h>

#include "VPSortByPhi_avx2.h"

namespace {
  // Permutation for avx2 compress, from https://github.com/lemire/simdprune
  alignas( 32 ) const uint32_t compress_mask256_epi32[] = {
      0, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 7, 0, 2, 3, 4, 5, 6, 7, 7, 2, 3, 4, 5, 6, 7, 7, 7, 0, 1, 3, 4, 5, 6,
      7, 7, 1, 3, 4, 5, 6, 7, 7, 7, 0, 3, 4, 5, 6, 7, 7, 7, 3, 4, 5, 6, 7, 7, 7, 7, 0, 1, 2, 4, 5, 6, 7, 7, 1, 2, 4, 5,
      6, 7, 7, 7, 0, 2, 4, 5, 6, 7, 7, 7, 2, 4, 5, 6, 7, 7, 7, 7, 0, 1, 4, 5, 6, 7, 7, 7, 1, 4, 5, 6, 7, 7, 7, 7, 0, 4,
      5, 6, 7, 7, 7, 7, 4, 5, 6, 7, 7, 7, 7, 7, 0, 1, 2, 3, 5, 6, 7, 7, 1, 2, 3, 5, 6, 7, 7, 7, 0, 2, 3, 5, 6, 7, 7, 7,
      2, 3, 5, 6, 7, 7, 7, 7, 0, 1, 3, 5, 6, 7, 7, 7, 1, 3, 5, 6, 7, 7, 7, 7, 0, 3, 5, 6, 7, 7, 7, 7, 3, 5, 6, 7, 7, 7,
      7, 7, 0, 1, 2, 5, 6, 7, 7, 7, 1, 2, 5, 6, 7, 7, 7, 7, 0, 2, 5, 6, 7, 7, 7, 7, 2, 5, 6, 7, 7, 7, 7, 7, 0, 1, 5, 6,
      7, 7, 7, 7, 1, 5, 6, 7, 7, 7, 7, 7, 0, 5, 6, 7, 7, 7, 7, 7, 5, 6, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 4, 6, 7, 7, 1, 2,
      3, 4, 6, 7, 7, 7, 0, 2, 3, 4, 6, 7, 7, 7, 2, 3, 4, 6, 7, 7, 7, 7, 0, 1, 3, 4, 6, 7, 7, 7, 1, 3, 4, 6, 7, 7, 7, 7,
      0, 3, 4, 6, 7, 7, 7, 7, 3, 4, 6, 7, 7, 7, 7, 7, 0, 1, 2, 4, 6, 7, 7, 7, 1, 2, 4, 6, 7, 7, 7, 7, 0, 2, 4, 6, 7, 7,
      7, 7, 2, 4, 6, 7, 7, 7, 7, 7, 0, 1, 4, 6, 7, 7, 7, 7, 1, 4, 6, 7, 7, 7, 7, 7, 0, 4, 6, 7, 7, 7, 7, 7, 4, 6, 7, 7,
      7, 7, 7, 7, 0, 1, 2, 3, 6, 7, 7, 7, 1, 2, 3, 6, 7, 7, 7, 7, 0, 2, 3, 6, 7, 7, 7, 7, 2, 3, 6, 7, 7, 7, 7, 7, 0, 1,
      3, 6, 7, 7, 7, 7, 1, 3, 6, 7, 7, 7, 7, 7, 0, 3, 6, 7, 7, 7, 7, 7, 3, 6, 7, 7, 7, 7, 7, 7, 0, 1, 2, 6, 7, 7, 7, 7,
      1, 2, 6, 7, 7, 7, 7, 7, 0, 2, 6, 7, 7, 7, 7, 7, 2, 6, 7, 7, 7, 7, 7, 7, 0, 1, 6, 7, 7, 7, 7, 7, 1, 6, 7, 7, 7, 7,
      7, 7, 0, 6, 7, 7, 7, 7, 7, 7, 6, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 4, 5, 7, 7, 1, 2, 3, 4, 5, 7, 7, 7, 0, 2, 3, 4,
      5, 7, 7, 7, 2, 3, 4, 5, 7, 7, 7, 7, 0, 1, 3, 4, 5, 7, 7, 7, 1, 3, 4, 5, 7, 7, 7, 7, 0, 3, 4, 5, 7, 7, 7, 7, 3, 4,
      5, 7, 7, 7, 7, 7, 0, 1, 2, 4, 5, 7, 7, 7, 1, 2, 4, 5, 7, 7, 7, 7, 0, 2, 4, 5, 7, 7, 7, 7, 2, 4, 5, 7, 7, 7, 7, 7,
      0, 1, 4, 5, 7, 7, 7, 7, 1, 4, 5, 7, 7, 7, 7, 7, 0, 4, 5, 7, 7, 7, 7, 7, 4, 5, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 5, 7,
      7, 7, 1, 2, 3, 5, 7, 7, 7, 7, 0, 2, 3, 5, 7, 7, 7, 7, 2, 3, 5, 7, 7, 7, 7, 7, 0, 1, 3, 5, 7, 7, 7, 7, 1, 3, 5, 7,
      7, 7, 7, 7, 0, 3, 5, 7, 7, 7, 7, 7, 3, 5, 7, 7, 7, 7, 7, 7, 0, 1, 2, 5, 7, 7, 7, 7, 1, 2, 5, 7, 7, 7, 7, 7, 0, 2,
      5, 7, 7, 7, 7, 7, 2, 5, 7, 7, 7, 7, 7, 7, 0, 1, 5, 7, 7, 7, 7, 7, 1, 5, 7, 7, 7, 7, 7, 7, 0, 5, 7, 7, 7, 7, 7, 7,
      5, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 4, 7, 7, 7, 1, 2, 3, 4, 7, 7, 7, 7, 0, 2, 3, 4, 7, 7, 7, 7, 2, 3, 4, 7, 7, 7,
      7, 7, 0, 1, 3, 4, 7, 7, 7, 7, 1, 3, 4, 7, 7, 7, 7, 7, 0, 3, 4, 7, 7, 7, 7, 7, 3, 4, 7, 7, 7, 7, 7, 7, 0, 1, 2, 4,
      7, 7, 7, 7, 1, 2, 4, 7, 7, 7, 7, 7, 0, 2, 4, 7, 7, 7, 7, 7, 2, 4, 7, 7, 7, 7, 7, 7, 0, 1, 4, 7, 7, 7, 7, 7, 1, 4,
      7, 7, 7, 7, 7, 7, 0, 4, 7, 7, 7, 7, 7, 7, 4, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 3, 7, 7, 7, 7, 1, 2, 3, 7, 7, 7, 7, 7,
      0, 2, 3, 7, 7, 7, 7, 7, 2, 3, 7, 7, 7, 7, 7, 7, 0, 1, 3, 7, 7, 7, 7, 7, 1, 3, 7, 7, 7, 7, 7, 7, 0, 3, 7, 7, 7, 7,
      7, 7, 3, 7, 7, 7, 7, 7, 7, 7, 0, 1, 2, 7, 7, 7, 7, 7, 1, 2, 7, 7, 7, 7, 7, 7, 0, 2, 7, 7, 7, 7, 7, 7, 2, 7, 7, 7,
      7, 7, 7, 7, 0, 1, 7, 7, 7, 7, 7, 7, 1, 7, 7, 7, 7, 7, 7, 7, 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 1,
      2, 3, 4, 5, 6, 6, 1, 2, 3, 4, 5, 6, 6, 6, 0, 2, 3, 4, 5, 6, 6, 6, 2, 3, 4, 5, 6, 6, 6, 6, 0, 1, 3, 4, 5, 6, 6, 6,
      1, 3, 4, 5, 6, 6, 6, 6, 0, 3, 4, 5, 6, 6, 6, 6, 3, 4, 5, 6, 6, 6, 6, 6, 0, 1, 2, 4, 5, 6, 6, 6, 1, 2, 4, 5, 6, 6,
      6, 6, 0, 2, 4, 5, 6, 6, 6, 6, 2, 4, 5, 6, 6, 6, 6, 6, 0, 1, 4, 5, 6, 6, 6, 6, 1, 4, 5, 6, 6, 6, 6, 6, 0, 4, 5, 6,
      6, 6, 6, 6, 4, 5, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 5, 6, 6, 6, 1, 2, 3, 5, 6, 6, 6, 6, 0, 2, 3, 5, 6, 6, 6, 6, 2, 3,
      5, 6, 6, 6, 6, 6, 0, 1, 3, 5, 6, 6, 6, 6, 1, 3, 5, 6, 6, 6, 6, 6, 0, 3, 5, 6, 6, 6, 6, 6, 3, 5, 6, 6, 6, 6, 6, 6,
      0, 1, 2, 5, 6, 6, 6, 6, 1, 2, 5, 6, 6, 6, 6, 6, 0, 2, 5, 6, 6, 6, 6, 6, 2, 5, 6, 6, 6, 6, 6, 6, 0, 1, 5, 6, 6, 6,
      6, 6, 1, 5, 6, 6, 6, 6, 6, 6, 0, 5, 6, 6, 6, 6, 6, 6, 5, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 4, 6, 6, 6, 1, 2, 3, 4,
      6, 6, 6, 6, 0, 2, 3, 4, 6, 6, 6, 6, 2, 3, 4, 6, 6, 6, 6, 6, 0, 1, 3, 4, 6, 6, 6, 6, 1, 3, 4, 6, 6, 6, 6, 6, 0, 3,
      4, 6, 6, 6, 6, 6, 3, 4, 6, 6, 6, 6, 6, 6, 0, 1, 2, 4, 6, 6, 6, 6, 1, 2, 4, 6, 6, 6, 6, 6, 0, 2, 4, 6, 6, 6, 6, 6,
      2, 4, 6, 6, 6, 6, 6, 6, 0, 1, 4, 6, 6, 6, 6, 6, 1, 4, 6, 6, 6, 6, 6, 6, 0, 4, 6, 6, 6, 6, 6, 6, 4, 6, 6, 6, 6, 6,
      6, 6, 0, 1, 2, 3, 6, 6, 6, 6, 1, 2, 3, 6, 6, 6, 6, 6, 0, 2, 3, 6, 6, 6, 6, 6, 2, 3, 6, 6, 6, 6, 6, 6, 0, 1, 3, 6,
      6, 6, 6, 6, 1, 3, 6, 6, 6, 6, 6, 6, 0, 3, 6, 6, 6, 6, 6, 6, 3, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 6, 6, 6, 6, 6, 1, 2,
      6, 6, 6, 6, 6, 6, 0, 2, 6, 6, 6, 6, 6, 6, 2, 6, 6, 6, 6, 6, 6, 6, 0, 1, 6, 6, 6, 6, 6, 6, 1, 6, 6, 6, 6, 6, 6, 6,
      0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 1, 2, 3, 4, 5, 5, 5, 1, 2, 3, 4, 5, 5, 5, 5, 0, 2, 3, 4, 5, 5,
      5, 5, 2, 3, 4, 5, 5, 5, 5, 5, 0, 1, 3, 4, 5, 5, 5, 5, 1, 3, 4, 5, 5, 5, 5, 5, 0, 3, 4, 5, 5, 5, 5, 5, 3, 4, 5, 5,
      5, 5, 5, 5, 0, 1, 2, 4, 5, 5, 5, 5, 1, 2, 4, 5, 5, 5, 5, 5, 0, 2, 4, 5, 5, 5, 5, 5, 2, 4, 5, 5, 5, 5, 5, 5, 0, 1,
      4, 5, 5, 5, 5, 5, 1, 4, 5, 5, 5, 5, 5, 5, 0, 4, 5, 5, 5, 5, 5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 0, 1, 2, 3, 5, 5, 5, 5,
      1, 2, 3, 5, 5, 5, 5, 5, 0, 2, 3, 5, 5, 5, 5, 5, 2, 3, 5, 5, 5, 5, 5, 5, 0, 1, 3, 5, 5, 5, 5, 5, 1, 3, 5, 5, 5, 5,
      5, 5, 0, 3, 5, 5, 5, 5, 5, 5, 3, 5, 5, 5, 5, 5, 5, 5, 0, 1, 2, 5, 5, 5, 5, 5, 1, 2, 5, 5, 5, 5, 5, 5, 0, 2, 5, 5,
      5, 5, 5, 5, 2, 5, 5, 5, 5, 5, 5, 5, 0, 1, 5, 5, 5, 5, 5, 5, 1, 5, 5, 5, 5, 5, 5, 5, 0, 5, 5, 5, 5, 5, 5, 5, 5, 5,
      5, 5, 5, 5, 5, 5, 0, 1, 2, 3, 4, 4, 4, 4, 1, 2, 3, 4, 4, 4, 4, 4, 0, 2, 3, 4, 4, 4, 4, 4, 2, 3, 4, 4, 4, 4, 4, 4,
      0, 1, 3, 4, 4, 4, 4, 4, 1, 3, 4, 4, 4, 4, 4, 4, 0, 3, 4, 4, 4, 4, 4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 0, 1, 2, 4, 4, 4,
      4, 4, 1, 2, 4, 4, 4, 4, 4, 4, 0, 2, 4, 4, 4, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 0, 1, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4,
      4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 1, 2, 3, 3, 3, 3, 3, 1, 2, 3, 3, 3, 3, 3, 3, 0, 2,
      3, 3, 3, 3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 3, 0, 1, 3, 3, 3, 3, 3, 3, 1, 3, 3, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 3, 3,
      3, 3, 3, 3, 3, 3, 3, 3, 0, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
      2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
} // namespace

namespace VeloTracking {

  inline __attribute__( ( always_inline ) ) void sort_by_phi( PlaneSoA* Pin, PlaneSoA* Pout, int* /* perms */,
                                                              int n_hits ) {
    for ( int i = 0; i < n_hits; i += 8 ) {
      __m256 key = compute_key( Pin, i );
      _mm256_store_ps( &( Pout->phi[i] ), key );
    }

    quicksort_avx2( Pout->phi, 0, n_hits - 1 );

    for ( int i = 0; i < n_hits; i += 8 ) {
      __m256 key = _mm256_load_ps( &( Pout->phi[i] ) );
      permute_hits( Pin, Pout, key, i );
    }
  }

  inline void set_used_avx2( PlaneSoA* P, int m, __m256i h ) {
    __m256i mask = _mm256_sllv_epi32( _mm256_set1_epi32( 1 ), _mm256_and_si256( h, _mm256_set1_epi32( 31 ) ) );
    __m256i perm = _mm256_srli_epi32( h, 5 );
    alignas( 32 ) int imask[8];
    alignas( 32 ) int iperm[8];

    _mm256_store_si256( (__m256i*)imask, mask );
    _mm256_store_si256( (__m256i*)iperm, perm );

    for ( int i = 0; i < 8; i++ ) {
      if ( ( m >> i ) & 1 ) { ( (uint32_t*)P->used )[iperm[i]] |= imask[i]; }
    }
  }

  inline __m256 diff_phi_avx2( __m256 phi1, __m256 phi2 ) { return _mm256_abs_ps( _mm256_sub_ps( phi1, phi2 ) ); }

  inline __m256 _mm256_maskmove_ps( int mask ) {
    __m256i vmask   = _mm256_set1_epi32( mask );
    __m256i bitmask = _mm256_setr_epi32( 1, 2, 4, 8, 16, 32, 64, 128 );
    vmask           = _mm256_and_si256( vmask, bitmask );
    vmask           = _mm256_cmpeq_epi32( vmask, bitmask );
    return _mm256_castsi256_ps( vmask );
  }

  inline __m256i _mm256_blendv_epi32( __m256i a, __m256i b, __m256 mask ) {
    return _mm256_castps_si256( _mm256_blendv_ps( _mm256_castsi256_ps( a ), _mm256_castsi256_ps( b ), mask ) );
  }

  inline int _mm256_mask_reduce_max_epi32( int mask, __m256i a ) {
    __m256i vmask   = _mm256_set1_epi32( mask );
    __m256i bitmask = _mm256_setr_epi32( 1, 2, 4, 8, 16, 32, 64, 128 );
    vmask           = _mm256_and_si256( vmask, bitmask );
    vmask           = _mm256_cmpeq_epi32( vmask, bitmask );
    a               = _mm256_and_si256( a, vmask );
    __m128i r       = _mm_max_epi32( _mm256_extractf128_si256( a, 0 ), _mm256_extractf128_si256( a, 1 ) );
    r               = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
    r               = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
    return _mm_extract_epi32( r, 0 );
  }

  inline float _mm256_reduce_max_ps( __m256 a ) {
    __m128 r = _mm_max_ps( _mm256_extractf128_ps( a, 0 ), _mm256_extractf128_ps( a, 1 ) );
    r        = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
    r        = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
    return _mm_cvtss_f32( r );
  }

  inline __attribute__( ( always_inline ) ) int TrackSeeding( PlaneSoA* P0, PlaneSoA* P1, PlaneSoA* P2,
                                                              LightTracksSoA* tracks, // Output track candidates
                                                              const int       t_insert_start ) {
    int n_tracks = 0;
    int start0   = 0;
    int start2   = 0;

    constexpr int MAX_H0_CANDIDATES_PER_H1 = 6;
    int16_t       insert_mask[MAX_H0_CANDIDATES_PER_H1];
    __m256        x0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m256        y0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m256        z0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m256i       h0_candidates[MAX_H0_CANDIDATES_PER_H1];

    __m256i inc = _mm256_set1_epi32( 8 );
    __m256i vh1 =
        _mm256_add_epi32( _mm256_set1_epi32( P1->offset ), _mm256_setr_epi32( -8, -7, -6, -5, -4, -3, -2, -1 ) );
    for ( int h1 = 0; h1 < P1->n_hits; h1 += 8 ) {
      vh1            = _mm256_add_epi32( vh1, inc );
      uint8_t used_1 = ( (uint8_t*)P1->used )[h1 / 8];
      if ( ( h1 + 8 ) > P1->n_hits ) used_1 |= 0xFF << ( P1->n_hits & 7 );
      if ( used_1 == 0xFF ) continue;

      const __m256 x1    = _mm256_load_ps( &( P1->Gx[h1] ) );
      const __m256 y1    = _mm256_load_ps( &( P1->Gy[h1] ) );
      const __m256 z1    = _mm256_load_ps( &( P1->Gz[h1] ) );
      const __m256 phi1  = _mm256_load_ps( &( P1->phi[h1] ) );
      const float  phi1f = ( ( h1 + 8 ) > P1->n_hits ) ? P1->phi[P1->n_hits - 1] : P1->phi[h1 + 7]; // largest phi in h1

      __m256  bestFit = _mm256_set1_ps( 0.1f /*TrackParams::max_scatter_seeding*/ );
      int     bestH0  = 0;
      __m256i bestH2  = _mm256_setzero_si256();
      __m256i vh0     = _mm256_setzero_si256();
      __m256i vh2     = _mm256_setzero_si256();

      __m256 bestX0 = _mm256_setzero_ps();
      __m256 bestY0 = _mm256_setzero_ps();
      __m256 bestZ0 = _mm256_setzero_ps();
      __m256 bestX2 = _mm256_setzero_ps();
      __m256 bestY2 = _mm256_setzero_ps();
      __m256 bestZ2 = _mm256_setzero_ps();

      for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) insert_mask[i] = 0; // reset candidates masks

      // Fill h0 candidates TETRIS-style
      for ( int h0 = start0; h0 < P0->n_hits; h0++ ) {
        if ( is_used( P0, h0 ) ) continue;

        const __m256i cid0 = _mm256_set1_epi32( P0->offset + h0 );
        const __m256  x0   = _mm256_set1_ps( P0->Gx[h0] );
        const __m256  y0   = _mm256_set1_ps( P0->Gy[h0] );
        const __m256  z0   = _mm256_set1_ps( P0->Gz[h0] );

        const float  phi0f = P0->phi[h0];
        const __m256 phi0  = _mm256_set1_ps( phi0f );
        const __m256 dphi  = diff_phi_avx2( phi1, phi0 );

        float phi_window = TrackParams::phi_window_param( P0->Gz[h0] );
        int   m_phi      = _mm256_movemask_ps( _mm256_cmp_ps( dphi, _mm256_set1_ps( phi_window ), _CMP_LE_OS ) );
        m_phi &= ~used_1;
        if ( m_phi == 0 ) {
          if ( phi1f < phi0f ) break;
          start0 = h0 + 1;
          continue;
        }

        // h0 is a candidate
        for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) {
          int can_store = m_phi & ( ~insert_mask[i] );
          insert_mask[i] |= m_phi;

          __m256 mask_can_store = _mm256_maskmove_ps( can_store );

          x0_candidates[i] = _mm256_blendv_ps( x0_candidates[i], x0, mask_can_store );
          y0_candidates[i] = _mm256_blendv_ps( y0_candidates[i], y0, mask_can_store );
          z0_candidates[i] = _mm256_blendv_ps( z0_candidates[i], z0, mask_can_store );
          h0_candidates[i] = _mm256_blendv_epi32( h0_candidates[i], cid0, mask_can_store );

          m_phi &= ~can_store;
          if ( m_phi == 0 ) break;
        }
      }

      int next_start2 = start2; // start2 cannot be updated while all h0 candidates havent been processed
      for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) {
        int m_phi = insert_mask[i];
        if ( m_phi == 0 ) break;

        const __m256  x0 = x0_candidates[i];
        const __m256  y0 = y0_candidates[i];
        const __m256  z0 = z0_candidates[i];
        const __m256i h0 = h0_candidates[i];

        for ( int h2 = start2; h2 < P2->n_hits; h2++ ) {
          const float  phi2f = P2->phi[h2];
          const __m256 phi2  = _mm256_set1_ps( phi2f );
          const __m256 dphi  = diff_phi_avx2( phi1, phi2 );

          float phi_window = TrackParams::phi_window_param( P2->Gz[h2] );
          int   m_phi2     = _mm256_movemask_ps( _mm256_cmp_ps( dphi, _mm256_set1_ps( phi_window ), _CMP_LE_OS ) );
          m_phi2 &= ~used_1;
          if ( m_phi2 == 0 ) {
            if ( phi1f < phi2f ) break;
            next_start2 = h2 + 1;
            continue;
          }

          m_phi2 &= m_phi;
          if ( m_phi2 == 0 ) continue;

          const __m256i cid2 = _mm256_set1_epi32( P2->offset + h2 );
          const __m256  x2   = _mm256_set1_ps( P2->Gx[h2] );
          const __m256  y2   = _mm256_set1_ps( P2->Gy[h2] );
          const __m256  z2   = _mm256_set1_ps( P2->Gz[h2] );

          const __m256 vr = _mm256_div_ps( _mm256_sub_ps( z2, z0 ), _mm256_sub_ps( z1, z0 ) );
          const __m256 x  = _mm256_fmadd_ps( _mm256_sub_ps( x1, x0 ), vr, x0 );
          const __m256 y  = _mm256_fmadd_ps( _mm256_sub_ps( y1, y0 ), vr, y0 );

          const __m256 dx = _mm256_sub_ps( x, x2 );
          const __m256 dy = _mm256_sub_ps( y, y2 );
          // const __m256 dz      = _mm256_sub_ps( z2, z1 );
          // const __m256 scDen2  = _mm256_mul_ps( dz, dz );
          const __m256 scatter =
              _mm256_fmadd_ps( dx, dx, _mm256_mul_ps( dy, dy ) ); //_mm256_div_ps( _mm256_fmadd_ps( dx, dx,
                                                                  //_mm256_mul_ps( dy, dy ) ), scDen2 );

          /*__m256 m_tol = _mm256_and_ps(
              _mm256_cmp_ps( _mm256_abs_ps( dx ), _mm256_set1_ps( TrackParams::tolerance ), _CMP_LT_OS ),
              _mm256_cmp_ps( _mm256_abs_ps( dy ), _mm256_set1_ps( TrackParams::tolerance ), _CMP_LT_OS ) );*/

          int m_bestfit =
              _mm256_movemask_ps( /*_mm256_and_ps( m_tol,*/ _mm256_cmp_ps( scatter, bestFit, _CMP_LT_OS ) ); // );
          m_bestfit &= m_phi2;

          if ( m_bestfit == 0 ) continue;

          __m256 mask_bestfit = _mm256_maskmove_ps( m_bestfit );

          bestFit = _mm256_blendv_ps( bestFit, scatter, mask_bestfit );
          bestX0  = _mm256_blendv_ps( bestX0, x0, mask_bestfit );
          bestY0  = _mm256_blendv_ps( bestY0, y0, mask_bestfit );
          bestZ0  = _mm256_blendv_ps( bestZ0, z0, mask_bestfit );
          bestX2  = _mm256_blendv_ps( bestX2, x2, mask_bestfit );
          bestY2  = _mm256_blendv_ps( bestY2, y2, mask_bestfit );
          bestZ2  = _mm256_blendv_ps( bestZ2, z2, mask_bestfit );
          bestH2  = _mm256_blendv_epi32( bestH2, _mm256_set1_epi32( h2 ), mask_bestfit );
          vh0     = _mm256_blendv_epi32( vh0, h0, mask_bestfit );
          vh2     = _mm256_blendv_epi32( vh2, cid2, mask_bestfit );

          bestH0 |= m_bestfit;
        } // h2
      }   // n_0_candidates
      start2 = next_start2;

      // Clone killing: require that h2 are different => dont do in avx2

      if ( bestH0 == 0 ) continue;

      __m256i perm = _mm256_load_si256( (const __m256i*)compress_mask256_epi32 + ( bestH0 ^ 0xFF ) );
      int     i    = t_insert_start + n_tracks;

      _mm256_storeu_ps( &( tracks->sum_scatter[i] ), _mm256_permutevar8x32_ps( bestFit, perm ) );

      _mm256_storeu_ps( &( tracks->x0[i] ), _mm256_permutevar8x32_ps( bestX0, perm ) );
      _mm256_storeu_ps( &( tracks->y0[i] ), _mm256_permutevar8x32_ps( bestY0, perm ) );
      _mm256_storeu_ps( &( tracks->z0[i] ), _mm256_permutevar8x32_ps( bestZ0, perm ) );

      _mm256_storeu_ps( &( tracks->x1[i] ), _mm256_permutevar8x32_ps( x1, perm ) );
      _mm256_storeu_ps( &( tracks->y1[i] ), _mm256_permutevar8x32_ps( y1, perm ) );
      _mm256_storeu_ps( &( tracks->z1[i] ), _mm256_permutevar8x32_ps( z1, perm ) );

      _mm256_storeu_ps( &( tracks->x2[i] ), _mm256_permutevar8x32_ps( bestX2, perm ) );
      _mm256_storeu_ps( &( tracks->y2[i] ), _mm256_permutevar8x32_ps( bestY2, perm ) );
      _mm256_storeu_ps( &( tracks->z2[i] ), _mm256_permutevar8x32_ps( bestZ2, perm ) );

      _mm256_storeu_ps( &( tracks->phi2[i] ), _mm256_permutevar8x32_ps( phi1, perm ) ); // Cheat a little: phi1 ~= phi2

      _mm256_storeu_si256( (__m256i*)&( tracks->n_hits[i] ), _mm256_set1_epi32( 3 ) ); // save permute
      _mm256_storeu_si256( (__m256i*)&( tracks->skipped[i] ), _mm256_setzero_si256() );

      if ( TrackParams::save_LHCbIds ) {
        _mm256_storeu_si256( (__m256i*)&( tracks->cId[i] ), _mm256_permutevar8x32_epi32( vh0, perm ) );
        _mm256_storeu_si256( (__m256i*)&( tracks->cId[MAX_TRACKS_PER_EVENT + i] ),
                             _mm256_permutevar8x32_epi32( vh1, perm ) );
        _mm256_storeu_si256( (__m256i*)&( tracks->cId[2 * MAX_TRACKS_PER_EVENT + i] ),
                             _mm256_permutevar8x32_epi32( vh2, perm ) );
      }

      n_tracks += _mm_popcnt_u32( bestH0 );
      ( (uint8_t*)P1->used )[h1 / 8] |= bestH0;
      set_used_avx2( P2, bestH0, bestH2 );
    } // h1

    return n_tracks;
  }

  inline __attribute__( ( always_inline ) ) void TrackForwarding( LightTracksSoA* tracks, // Track candidates
                                                                  const int n_tracks, PlaneSoA* P2,
                                                                  LightTracksSoA* tracks_forwarded, int& n_forwarded,
                                                                  LightTracksSoA* tracks_finalized, int& n_finalized ) {
    for ( int t = 0; t < n_tracks; t += 8 ) {
      int mask = ( ( t + 8 ) > n_tracks ) ? ~( 0xFF << ( n_tracks & 7 ) ) : 0xFF;

      __m256 x0   = _mm256_load_ps( &( tracks->x1[t] ) );
      __m256 y0   = _mm256_load_ps( &( tracks->y1[t] ) );
      __m256 z0   = _mm256_load_ps( &( tracks->z1[t] ) );
      __m256 x1   = _mm256_load_ps( &( tracks->x2[t] ) );
      __m256 y1   = _mm256_load_ps( &( tracks->y2[t] ) );
      __m256 z1   = _mm256_load_ps( &( tracks->z2[t] ) );
      __m256 phi1 = _mm256_load_ps( &( tracks->phi2[t] ) );

      float phi1f = _mm256_reduce_max_ps( phi1 ); // largest phi in tracks

      //__m256  bestFit = _mm256_set1_ps( TrackParams::max_scatter_forwarding );
      __m256  bestFit = _mm256_set1_ps( 0.1f );
      int     bestH0  = 0;
      __m256i bestH2  = _mm256_setzero_si256();
      __m256  bestX2  = x1;
      __m256  bestY2  = y1;
      __m256  bestZ2  = z1;
      __m256  bestPhi = phi1;
      __m256i vh2     = _mm256_setzero_si256();

      for ( int h2 = 0; h2 < P2->n_hits; h2++ ) {
        if ( is_used( P2, h2 ) ) continue;

        float  phi2f = P2->phi[h2];
        __m256 phi2  = _mm256_set1_ps( phi2f );
        __m256 dphi  = diff_phi_avx2( phi1, phi2 );

        float phi_window = TrackParams::phi_window_param( P2->Gz[h2] );
        int   m_phi2     = _mm256_movemask_ps( _mm256_cmp_ps( dphi, _mm256_set1_ps( phi_window ), _CMP_LE_OS ) );
        m_phi2 &= mask;
        if ( m_phi2 == 0 ) {
          if ( phi1f < phi2f ) break;
          continue; // cannot restart from start2, because tracks not ordered
        }
        // int m_phi2 = mask;

        __m256i cid2 = _mm256_set1_epi32( P2->offset + h2 );
        __m256  x2   = _mm256_set1_ps( P2->Gx[h2] );
        __m256  y2   = _mm256_set1_ps( P2->Gy[h2] );
        __m256  z2   = _mm256_set1_ps( P2->Gz[h2] );

        __m256 vr = _mm256_div_ps( _mm256_sub_ps( z2, z0 ), _mm256_sub_ps( z1, z0 ) );
        __m256 x  = _mm256_fmadd_ps( _mm256_sub_ps( x1, x0 ), vr, x0 );
        __m256 y  = _mm256_fmadd_ps( _mm256_sub_ps( y1, y0 ), vr, y0 );

        __m256 dx = _mm256_sub_ps( x, x2 );
        __m256 dy = _mm256_sub_ps( y, y2 );
        //__m256       dz      = _mm256_sub_ps( z2, z1 );
        // const __m256 scDen2  = _mm256_mul_ps( dz, dz );
        // const __m256 scatter = _mm256_div_ps( _mm256_fmadd_ps( dx, dx, _mm256_mul_ps( dy, dy ) ), scDen2 );
        const __m256 scatter = _mm256_fmadd_ps( dx, dx, _mm256_mul_ps( dy, dy ) );

        /*__m256 m_tol =
            _mm256_and_ps( _mm256_cmp_ps( _mm256_abs_ps( dx ), _mm256_set1_ps( TrackParams::tolerance ), _CMP_LT_OS ),
                           _mm256_cmp_ps( _mm256_abs_ps( dy ), _mm256_set1_ps( TrackParams::tolerance ), _CMP_LT_OS )
           );*/

        // int m_bestfit = _mm256_movemask_ps( _mm256_and_ps( m_tol, _mm256_cmp_ps( scatter, bestFit, _CMP_LT_OS ) ) );
        int m_bestfit = _mm256_movemask_ps( _mm256_cmp_ps( scatter, bestFit, _CMP_LT_OS ) );
        m_bestfit &= m_phi2;

        if ( m_bestfit == 0 ) continue;

        __m256 mask_bestfit = _mm256_maskmove_ps( m_bestfit );

        bestFit = _mm256_blendv_ps( bestFit, scatter, mask_bestfit );
        bestX2  = _mm256_blendv_ps( bestX2, x2, mask_bestfit );
        bestY2  = _mm256_blendv_ps( bestY2, y2, mask_bestfit );
        bestZ2  = _mm256_blendv_ps( bestZ2, z2, mask_bestfit );
        bestPhi = _mm256_blendv_ps( bestPhi, phi2, mask_bestfit );
        bestH2  = _mm256_blendv_epi32( bestH2, _mm256_set1_epi32( h2 ), mask_bestfit );
        vh2     = _mm256_blendv_epi32( vh2, cid2, mask_bestfit );

        bestH0 |= m_bestfit;
      }

      __m256 mask_bestH0 = _mm256_maskmove_ps( bestH0 );

      // Finish loading tracks
      __m256  tx0     = _mm256_load_ps( &( tracks->x0[t] ) );
      __m256  ty0     = _mm256_load_ps( &( tracks->y0[t] ) );
      __m256  tz0     = _mm256_load_ps( &( tracks->z0[t] ) );
      __m256i n_hits  = _mm256_load_si256( (const __m256i*)&( tracks->n_hits[t] ) );
      __m256i skipped = _mm256_load_si256( (const __m256i*)&( tracks->skipped[t] ) );
      __m256  ssc     = _mm256_load_ps( &( tracks->sum_scatter[t] ) );

      // increment or reset to 0
      skipped = _mm256_andnot_si256( _mm256_castps_si256( mask_bestH0 ),
                                     _mm256_add_epi32( skipped, _mm256_set1_epi32( 1 ) ) );

      // increment only if we found a hit
      n_hits = _mm256_blendv_epi32( n_hits, _mm256_add_epi32( n_hits, _mm256_set1_epi32( 1 ) ), mask_bestH0 );
      ssc    = _mm256_blendv_ps( ssc, _mm256_add_ps( ssc, bestFit ), mask_bestH0 );

      x1 = _mm256_blendv_ps( x0, x1, mask_bestH0 );
      y1 = _mm256_blendv_ps( y0, y1, mask_bestH0 );
      z1 = _mm256_blendv_ps( z0, z1, mask_bestH0 );

      int m_forward = _mm256_movemask_ps( _mm256_castsi256_ps(
          _mm256_cmpgt_epi32( _mm256_set1_epi32( TrackParams::max_allowed_skip + 1 ), skipped ) ) );
      m_forward &= mask;

      // Mark used
      set_used_avx2( P2, bestH0 & m_forward, bestH2 );

      // Forward tracks
      __m256i perm = _mm256_load_si256( (const __m256i*)compress_mask256_epi32 + ( m_forward ^ 0xFF ) );
      int     i    = n_forwarded;

      _mm256_storeu_ps( &( tracks_forwarded->sum_scatter[i] ), _mm256_permutevar8x32_ps( ssc, perm ) );

      _mm256_storeu_ps( &( tracks_forwarded->x0[i] ), _mm256_permutevar8x32_ps( tx0, perm ) );
      _mm256_storeu_ps( &( tracks_forwarded->y0[i] ), _mm256_permutevar8x32_ps( ty0, perm ) );
      _mm256_storeu_ps( &( tracks_forwarded->z0[i] ), _mm256_permutevar8x32_ps( tz0, perm ) );

      _mm256_storeu_ps( &( tracks_forwarded->x1[i] ), _mm256_permutevar8x32_ps( x1, perm ) );
      _mm256_storeu_ps( &( tracks_forwarded->y1[i] ), _mm256_permutevar8x32_ps( y1, perm ) );
      _mm256_storeu_ps( &( tracks_forwarded->z1[i] ), _mm256_permutevar8x32_ps( z1, perm ) );

      _mm256_storeu_ps( &( tracks_forwarded->x2[i] ), _mm256_permutevar8x32_ps( bestX2, perm ) );
      _mm256_storeu_ps( &( tracks_forwarded->y2[i] ), _mm256_permutevar8x32_ps( bestY2, perm ) );
      _mm256_storeu_ps( &( tracks_forwarded->z2[i] ), _mm256_permutevar8x32_ps( bestZ2, perm ) );

      _mm256_storeu_ps( &( tracks_forwarded->phi2[i] ), _mm256_permutevar8x32_ps( bestPhi, perm ) );

      _mm256_storeu_si256( (__m256i*)&( tracks_forwarded->n_hits[i] ), _mm256_permutevar8x32_epi32( n_hits, perm ) );
      _mm256_storeu_si256( (__m256i*)&( tracks_forwarded->skipped[i] ), _mm256_permutevar8x32_epi32( skipped, perm ) );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm256_mask_reduce_max_epi32( m_forward, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m256i cid      = _mm256_load_si256( (const __m256i*)&( tracks->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          __m256  push_hit = _mm256_and_ps(
              _mm256_castsi256_ps( _mm256_cmpeq_epi32( n_hits, _mm256_set1_epi32( j + 1 ) ) ), mask_bestH0 );
          cid = _mm256_blendv_epi32( cid, vh2, push_hit );
          _mm256_storeu_si256( (__m256i*)&( tracks_forwarded->cId[j * MAX_TRACKS_PER_EVENT + i] ),
                               _mm256_permutevar8x32_epi32( cid, perm ) );
        }
      }

      n_forwarded += _mm_popcnt_u32( m_forward );

      // Finalize track
      int m_final = _mm256_movemask_ps( _mm256_or_ps(
          _mm256_castsi256_ps( _mm256_cmpgt_epi32( n_hits, _mm256_set1_epi32( TrackParams::min_hit_per_track - 1 ) ) ),
          _mm256_cmp_ps( ssc, _mm256_set1_ps( TrackParams::max_scatter_3hits ), _CMP_LT_OS ) ) );
      m_final &= mask & ~m_forward;

      if ( m_final == 0 ) continue; // Nothing to finalize

      perm = _mm256_load_si256( (const __m256i*)compress_mask256_epi32 + ( m_final ^ 0xFF ) );
      i    = n_finalized;

      _mm256_storeu_ps( &( tracks_finalized->sum_scatter[i] ), _mm256_permutevar8x32_ps( ssc, perm ) );

      _mm256_storeu_ps( &( tracks_finalized->x0[i] ), _mm256_permutevar8x32_ps( tx0, perm ) );
      _mm256_storeu_ps( &( tracks_finalized->y0[i] ), _mm256_permutevar8x32_ps( ty0, perm ) );
      _mm256_storeu_ps( &( tracks_finalized->z0[i] ), _mm256_permutevar8x32_ps( tz0, perm ) );

      _mm256_storeu_ps( &( tracks_finalized->x1[i] ), _mm256_permutevar8x32_ps( x1, perm ) );
      _mm256_storeu_ps( &( tracks_finalized->y1[i] ), _mm256_permutevar8x32_ps( y1, perm ) );
      _mm256_storeu_ps( &( tracks_finalized->z1[i] ), _mm256_permutevar8x32_ps( z1, perm ) );

      _mm256_storeu_ps( &( tracks_finalized->x2[i] ), _mm256_permutevar8x32_ps( bestX2, perm ) );
      _mm256_storeu_ps( &( tracks_finalized->y2[i] ), _mm256_permutevar8x32_ps( bestY2, perm ) );
      _mm256_storeu_ps( &( tracks_finalized->z2[i] ), _mm256_permutevar8x32_ps( bestZ2, perm ) );

      _mm256_storeu_ps( &( tracks_finalized->phi2[i] ), _mm256_permutevar8x32_ps( bestPhi, perm ) );

      _mm256_storeu_si256( (__m256i*)&( tracks_finalized->n_hits[i] ), _mm256_permutevar8x32_epi32( n_hits, perm ) );
      _mm256_storeu_si256( (__m256i*)&( tracks_finalized->skipped[i] ), _mm256_permutevar8x32_epi32( skipped, perm ) );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm256_mask_reduce_max_epi32( m_final, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m256i cid = _mm256_load_si256( (const __m256i*)&( tracks->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          _mm256_storeu_si256( (__m256i*)&( tracks_finalized->cId[j * MAX_TRACKS_PER_EVENT + i] ),
                               _mm256_permutevar8x32_epi32( cid, perm ) );
        }
      }

      n_finalized += _mm_popcnt_u32( m_final );
    }
  }

  inline __attribute__( ( always_inline ) ) void copy_remaining( LightTracksSoA* tracks_candidates, int n_candidates,
                                                                 LightTracksSoA* tracks, int& n_tracks_output ) {
    __m256i min_hit_per_track = _mm256_set1_epi32( TrackParams::min_hit_per_track - 1 );
    __m256  max_scatter_3hits = _mm256_set1_ps( TrackParams::max_scatter_3hits );
    for ( int t = 0; t < n_candidates; t += 8 ) {
      __m256i n_hits = _mm256_load_si256( (const __m256i*)&( tracks_candidates->n_hits[t] ) );
      __m256  ssc    = _mm256_load_ps( &( tracks_candidates->sum_scatter[t] ) );

      __m256 final_mask = _mm256_or_ps( _mm256_castsi256_ps( _mm256_cmpgt_epi32( n_hits, min_hit_per_track ) ),
                                        _mm256_cmp_ps( ssc, max_scatter_3hits, _CMP_LT_OS ) );

      int m_final = _mm256_movemask_ps( final_mask );
      m_final &= ( ( t + 8 ) > n_candidates ) ? ~( 0xFF << ( n_candidates & 7 ) ) : 0xFF;

      __m256i perm = _mm256_load_si256( (const __m256i*)compress_mask256_epi32 + ( m_final ^ 0xFF ) );

      int i = n_tracks_output;
      _mm256_storeu_ps( &( tracks->sum_scatter[i] ), _mm256_permutevar8x32_ps( ssc, perm ) );

      _mm256_storeu_ps( &( tracks->x0[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->x0[t] ) ), perm ) );
      _mm256_storeu_ps( &( tracks->y0[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->y0[t] ) ), perm ) );
      _mm256_storeu_ps( &( tracks->z0[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->z0[t] ) ), perm ) );

      _mm256_storeu_ps( &( tracks->x1[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->x1[t] ) ), perm ) );
      _mm256_storeu_ps( &( tracks->y1[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->y1[t] ) ), perm ) );
      _mm256_storeu_ps( &( tracks->z1[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->z1[t] ) ), perm ) );

      _mm256_storeu_ps( &( tracks->x2[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->x2[t] ) ), perm ) );
      _mm256_storeu_ps( &( tracks->y2[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->y2[t] ) ), perm ) );
      _mm256_storeu_ps( &( tracks->z2[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->z2[t] ) ), perm ) );

      _mm256_storeu_ps( &( tracks->phi2[i] ),
                        _mm256_permutevar8x32_ps( _mm256_load_ps( &( tracks_candidates->phi2[t] ) ), perm ) );

      _mm256_storeu_si256( (__m256i*)&( tracks->n_hits[i] ), _mm256_permutevar8x32_epi32( n_hits, perm ) );
      _mm256_storeu_si256( (__m256i*)&( tracks->skipped[i] ),
                           _mm256_permutevar8x32_epi32(
                               _mm256_load_si256( (const __m256i*)&( tracks_candidates->skipped[t] ) ), perm ) );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm256_mask_reduce_max_epi32( m_final, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m256i cid = _mm256_load_si256( (const __m256i*)&( tracks_candidates->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          _mm256_storeu_si256( (__m256i*)&( tracks->cId[j * MAX_TRACKS_PER_EVENT + i] ),
                               _mm256_permutevar8x32_epi32( cid, perm ) );
        }
      }

      n_tracks_output += _mm_popcnt_u32( m_final );
    }
  }
} // namespace VeloTracking
