/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from Gaudi
#include "PrTrackConverter.h"

DECLARE_COMPONENT( PrTrackConverter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrTrackConverter::PrTrackConverter( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"InputTracksLocation", ""}, KeyValue{"OutKeyedTrackLocation", ""} ) {}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks PrTrackConverter::operator()( const std::vector<LHCb::Track>& inputTracks ) const {
  // Loop over the Tracks
  LHCb::Tracks OutputTracks;
  for ( auto& Source_Track : inputTracks ) {
    LHCb::Track* tr = new LHCb::Track;
    // Copy the track content in the new container
    tr->copy( Source_Track );
    OutputTracks.insert( tr );
  } // End loop over Tracks
  return OutputTracks;
}
