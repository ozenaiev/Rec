/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRCLUSTERSRESIDUAL_H
#define PRCLUSTERSRESIDUAL_H 1

// Include files
// from Gaudi
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/Track.h"
#include "FTDet/DeFTDetector.h"
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkerTool.h"
#include "MCInterfaces/IIdealStateCreator.h"
#include "MCInterfaces/ILHCbIDsToMCHits.h"
#include "MCInterfaces/ILHCbIDsToMCParticles.h"
#include "PrKernel/PrFTHitHandler.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
/** @class PrClustersResidual PrClustersResidual.h
 *  Make nTuples of Cluster vs MCHits and Hit content on track + Occupancy study possible ( also in PrPlotFTHits )
 *  -MCHitsLocation : Where to find the MCHits ( /Event/MC/FT/FTHits by default )
 *  -HitManagerName : The FTHitManage ( PrFTHitManager )
 *  -DoClusterResidual : Will produce nTuple of cluster  info vs MCHit contributing to it info
 *  -DoTrackStudy : Will produce nTuple filled by MCParticle with arrays of MCHits , Clusters, PrHit associated to it.
Possible to do study offline trough Root macros.
 *  -DecodeData : true or false, set to true if no Tracking algorithms ( forward or seeding are run ) . False by
default.
 *  -OnlyHasT: process only MCParticle which are reconstructible in the scifi
 *  -RemoveClones : Hack to remove duplicates MCHits / Clusters / PrHit in same layers ( set always to True )
 *  -DumpAllHits : produce nTuple filled by hit ( huge nTuples produced, set to False by Default )

 *  -Script example to use it :

def PrClusterResidual() :
    from Configurables import PrClustersResidual
    GaudiSequencer("RecoTrFastSeq").Members = []
    GaudiSequencer("RecoTrBestSeq").Members = []
    GaudiSequencer("RecoTrFastSeq").Members =
["PrPixelTracking","PrPixelStoreClusters","PrForwardTracking","PrHybridSeeding"] residual = PrClustersResidual(
DoClusterResidual = False , Occupancy = True ) #set properties residual.Occupancy = True #Make Plots of occupancy
    GaudiSequencer("MCLinksTrSeq").Members+= [residual]
appendPostConfigAction( PrClusterResidual )
#some MC linkers
from Configurables import MCParticle2MCHitAlg
FTAssoc = MCParticle2MCHitAlg( "MCP2FTMCHitAlg", MCHitPath = "MC/FT/Hits", OutputData = "/Event/MC/Particles2MCFTHits" )
DataOnDemandSvc().AlgMap[ "/Event/Link/MC/Particles2MCFTHits" ]    = FTAssoc
DataOnDemandSvc().NodeMap[ "/Event/Link" ]    = "DataObject"
DataOnDemandSvc().NodeMap[ "/Event/Link/MC" ] = "DataObject"
#########
HistogramPersistencySvc().OutputFile = "SciFiPlots_" + outHisto_name + ".root"
NTupleSvc().Output = ["FILE1 DATAFILE='Tuple_" + outHisto_name + ".root' TYP='ROOT' OPT='NEW'"]
#########


 *  @author renato quagliani
 *  @date   2015-01-29
 */
class PrClustersResidual : public GaudiTupleAlg {
public:
  /// Standard constructor
  PrClustersResidual( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  void HitEfficiency();

  /** @brief Create Ntuple to study clusters and PrHit generated.
   */
  void ClusterResidual( const PrFTHitHandler<PrHit>& hitHandler );

  /**@brief Create Ntuple containing PrHits for track study plus all the MCHits.
   */
  void TrackStudy( const PrFTHitHandler<PrHit>& hitHandler );

  /**@brief Create Ntuple containing all the PrHits in the event.
   */
  void DumpAllHits( const PrFTHitHandler<PrHit>& hitHandler );

  void Occupancy( const PrFTHitHandler<PrHit>& hitHandler );

  typedef std::vector<PrHit*> Hits;
  using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

  /// range of object for Hits
  typedef Gaudi::Range_<Hits> HitRange;

  std::vector<const LHCb::Track*> getTrack( const LHCb::LHCbID id, const std::string location );
  LHCb::FTLiteCluster             getLiteCluster( const LHCb::LHCbID id );

  Gaudi::Property<std::string> m_mcHitLocation{this, "MCHitsLocation", "/Event/MC/FT/Hits"};
  Gaudi::Property<std::string> m_ftClusterToParticleLinkerLocation{
      this, "FTClusterToParticleLinkerLocation", LHCb::FTLiteClusterLocation::Default + "WithSpillover"};
  Gaudi::Property<std::string> m_ftClusterToHitLinkerLocation{
      this, "FTClusterToHitLinkerLocation", LHCb::FTLiteClusterLocation::Default + "2MCHitsWithSpillover"};
  Gaudi::Property<std::string> m_hitManagerName{this, "HitManagerName", "PrFTHitManager"};

  Gaudi::Property<bool> m_debugTracking{this, "DebugTracking", false};
  Gaudi::Property<bool> m_doClusterResidual{this, "DoClusterResidual", false}; // Cluster residual tuple
  Gaudi::Property<bool> m_doTrackStudy{this, "DoTrackStudy", false};           // Produce tuple for track studies
  Gaudi::Property<bool> m_decodeData{this, "DecodeData",
                                     false}; // ask to decode data ( is False if other algorithms are runned
  Gaudi::Property<bool> m_onlyHasT{this, "OnlyHasT", false}; //(keep only MCParticles with hasT = true
  Gaudi::Property<bool> m_removeClones{this, "RemoveClones",
                                       true}; // Observed clones MCHits when processed ( not understood )
  Gaudi::Property<bool> m_dumpAllHits{this, "DumpAllHits", false}; // Dump in tuple for a given event all the hits
  Gaudi::Property<bool> m_Occupancy{this, "Occupancy", true};      // Produce Occupancy plot
  int                   m_nEvents           = 0;
  double                m_NClusters         = 0.;
  double                m_NMCHit            = 0.;
  double                m_NMCHit_inClusters = 0.;

  AnyDataHandle<FTLiteClusters> m_clusters{LHCb::FTLiteClusterLocation::Default, Gaudi::DataHandle::Reader, this};
  AnyDataHandle<PrFTHitHandler<PrHit>> m_HitsInTES{"/Event/FT/FTHits", Gaudi::DataHandle::Reader, this};

  // need the ftDet to produce the pseudochannel conversion numbering in occupancy plot for v61
  DeFTDetector* m_ftDet = nullptr;
};
#endif // PRCLUSTERSRESIDUAL_H
