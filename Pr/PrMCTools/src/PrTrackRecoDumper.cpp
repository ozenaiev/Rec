/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "PrTrackRecoDumper.h"
#include "Associators/Associators.h"
#include "Linker/LinkedTo.h"
#include "PrKernel/PrHit.h"
#include "PrKernel/UTHit.h"
#include <TString.h>

#include <utility>
//-----------------------------------------------------------------------------
// Implementation file for class : PrTrackRecoDumper
//
// 07-2018 Riccardo Cenci
// 21-11-2018 Giulia Tuci

// Based on PrTrackerDumper. Here, instead of MC information, we dump information of reconstructed tracks.
// If the variable isMatched==1, then the reconstructed track is correctly associated to a MC particle.

// Reconstructed tracks information:
/*
  p             : track momentum
  pt            : track trsansverse momentum
  eta           : track pseudorapidity
  ovtx_x        : track origin position X
  ovtx_y        : track origin position Y
  ovtx_z        : track origin position Z
  fromBeautyDecay : the track belongs to a decay chain with a b-quark hadron
  fromCharmDecay  : the track belongs to a decay chain with a c-quark hadron
  fromStrangeDecay : the track belongs to a decay chain with a s-quark hadron

  you can filter based on the simulated sample. If for instance you run over Bs->PhiPhi,
  you can filter the 4 kaons among all tracks requiring Bs PID for this variable
*/
/*  VELO related part
  "nVeloHits" : Number of VeloHits associated to the MCParticle
  Velo_x       : vector of x position for Velo  hits (size = nVeloHits)
  Velo_y       : vector of y position for Velo  hits (size = nVeloHits)
  Velo_z       : vector of z position for Velo  hits (size = nVeloHits)
  Velo_Module  : vector of ModuleID Velo  hits (size = nVeloHits)
  Velo_Sensor  : vector of SensorID Velo  hits (size = nVeloHits)
  Velo_Station : vector of StationID Velo  hits (size = nVeloHits)
  Velo_lhcbID  : vector of lhcbID Velo  hits (size = nVeloHits)
*/
/*  SciFi related part
  nFTHits   : Number of FTHits associated to the MCParticle
  FT_x      : vector of x(y=0) position for SciFi
  FT_z      : vector of z(y=0) position for SciFi hits
  FT_w      : vector of weight error   for SciFi hits
  FT_dxdy   : vector of slopes dxdy for SciFi hits
  FT_YMin   : vector of yMin  for SciFi hits
  FT_YMax   : vector of yMax  for SciFi hits
  FT_hitPlaneCode : vector of planeCode  for SciFi hits
  FT_hitzone      : vector of hitzone (up/down)  for SciFi hits
  FT_lhcbID       : vector of lhcbID  for SciFi hits
*/
/* UT related part
   nUTHits   : Number of UTHits associated to the MCParticle
  //---- see private members of UT:Hit in PrKernel package
  UT_cos
  UT_cosT
  UT_dxDy
  UT_lhcbID
  UT_planeCode
  UT_sinT
  UT_size
  UT_tanT
  UT_weight
  UT_xAtYEq0
  UT_xAtYMid
  UT_xMax
  UT_xMin
  UT_xT
  UT_yBegin
  UT_yEnd
  UT_yMax
  UT_yMid
  UT_yMin
  UT_zAtYEq0
*/
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrTrackRecoDumper )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================

PrTrackRecoDumper::PrTrackRecoDumper( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"TrackLocation", "Rec/Track/Velo"},
                 KeyValue{"VPLightClusterLocation", LHCb::VPClusterLocation::Light},
                 KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                 KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
                 KeyValue{"UTHitsLocation", UT::Info::HitLocation}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrTrackRecoDumper::initialize() {

  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  std::ostringstream oss;
  oss << "Dumper_recTracks"
      << ".root";
  TString filename = oss.str();
  // TFile *
  file = new TFile( filename.Data(), "RECREATE" );

  // TTree *
  tree = new TTree( "Hits_detectors", "Hits_detectors" );

  eventID                          = new int( 0 );
  p                                = new double( 0. );
  pt                               = new double( 0. );
  px                               = new double( 0. );
  py                               = new double( 0. );
  pz                               = new double( 0. );
  eta                              = new double( 0. );
  phi                              = new double( 0. );
  ovtx_x                           = new double( 0. );
  ovtx_y                           = new double( 0. );
  ovtx_z                           = new double( 0. );
  pid                              = new int( 0 );
  key                              = new int( 0 );
  ghostProb                        = new double( 0. );
  chi2                             = new double( 0. );
  ndof                             = new int( 0 );
  fromBeautyDecay                  = new bool( false );
  fromCharmDecay                   = new bool( false );
  fromStrangeDecay                 = new bool( false );
  isMatched                        = new bool( false );
  nVeloHits                        = new int( 0 );
  const unsigned int max_velo_hits = 50;

  Velo_x       = new std::vector<float>( max_velo_hits, 0. );
  Velo_y       = new std::vector<float>( max_velo_hits, 0. );
  Velo_z       = new std::vector<float>( max_velo_hits, 0. );
  Velo_Module  = new std::vector<int>( max_velo_hits, 0 );
  Velo_Sensor  = new std::vector<int>( max_velo_hits, 0 );
  Velo_Station = new std::vector<int>( max_velo_hits, 0 );
  Velo_lhcbID  = new std::vector<unsigned int>( max_velo_hits, 0 );

  tree->Branch( "eventID", eventID );
  tree->Branch( "p", p );
  tree->Branch( "pt", pt );
  tree->Branch( "px", px );
  tree->Branch( "py", py );
  tree->Branch( "pz", pz );
  tree->Branch( "eta", eta );
  tree->Branch( "phi", phi );
  tree->Branch( "ovtx_x", ovtx_x );
  tree->Branch( "ovtx_y", ovtx_y );
  tree->Branch( "ovtx_z", ovtx_z );
  tree->Branch( "pid", pid );
  tree->Branch( "key", key );
  tree->Branch( "ghostProb", ghostProb );
  tree->Branch( "chi2", chi2 );
  tree->Branch( "ndof", ndof );
  tree->Branch( "fromBeautyDecay", fromBeautyDecay );
  tree->Branch( "fromCharmDecay", fromCharmDecay );
  tree->Branch( "fromStrangeDecay", fromStrangeDecay );
  tree->Branch( "isMatched", isMatched );

  tree->Branch( "nVeloHits", nVeloHits );
  tree->Branch( "Velo_x", Velo_x );
  tree->Branch( "Velo_y", Velo_y );
  tree->Branch( "Velo_z", Velo_z );
  tree->Branch( "Velo_Module", Velo_Module );
  tree->Branch( "Velo_Sensor", Velo_Sensor );
  tree->Branch( "Velo_Station", Velo_Station );
  tree->Branch( "Velo_lhcbID", Velo_lhcbID );

  int maxFThits   = 1000;
  FT_hitx         = new std::vector<float>( maxFThits, 0 );
  FT_hitz         = new std::vector<float>( maxFThits, 0 );
  FT_hitw         = new std::vector<float>( maxFThits, 0 );
  FT_hitDXDY      = new std::vector<float>( maxFThits, 0 );
  FT_hitYMin      = new std::vector<float>( maxFThits, 0 );
  FT_hitYMax      = new std::vector<float>( maxFThits, 0 );
  FT_hitPlaneCode = new std::vector<int>( maxFThits, 0 );
  FT_hitzone      = new std::vector<int>( maxFThits, 0 );
  FT_lhcbID       = new std::vector<unsigned int>( maxFThits, 0 );

  nFTHits = new int( 0 );

  tree->Branch( "nFTHits", nFTHits );
  tree->Branch( "FT_x", FT_hitx );
  tree->Branch( "FT_z", FT_hitz );
  tree->Branch( "FT_w", FT_hitw );
  tree->Branch( "FT_dxdy", FT_hitDXDY );
  tree->Branch( "FT_YMin", FT_hitYMin );
  tree->Branch( "FT_YMax", FT_hitYMax );
  tree->Branch( "FT_hitPlaneCode", FT_hitPlaneCode );
  tree->Branch( "FT_hitzone", FT_hitzone );
  tree->Branch( "FT_lhcbID", FT_lhcbID );

  int maxUThits = 1000;
  UT_cos        = new std::vector<float>( maxUThits, 0 );
  UT_cosT       = new std::vector<float>( maxUThits, 0 );
  UT_dxDy       = new std::vector<float>( maxUThits, 0 );
  UT_lhcbID     = new std::vector<unsigned int>( maxUThits, 0 );
  UT_planeCode  = new std::vector<int>( maxUThits, 0 );
  UT_sinT       = new std::vector<float>( maxUThits, 0 );
  UT_size       = new std::vector<int>( maxUThits, 0 );
  UT_tanT       = new std::vector<float>( maxUThits, 0 );
  UT_weight     = new std::vector<float>( maxUThits, 0 );
  UT_xAtYEq0    = new std::vector<float>( maxUThits, 0 );
  UT_xAtYMid    = new std::vector<float>( maxUThits, 0 );
  UT_xT         = new std::vector<float>( maxUThits, 0 );
  UT_xMax       = new std::vector<float>( maxUThits, 0 );
  UT_xMin       = new std::vector<float>( maxUThits, 0 );
  UT_yBegin     = new std::vector<float>( maxUThits, 0 );
  UT_yEnd       = new std::vector<float>( maxUThits, 0 );
  UT_yMax       = new std::vector<float>( maxUThits, 0 );
  UT_yMid       = new std::vector<float>( maxUThits, 0 );
  UT_yMin       = new std::vector<float>( maxUThits, 0 );
  UT_zAtYEq0    = new std::vector<float>( maxUThits, 0 );

  nUTHits = new int( 0 );

  tree->Branch( "nUTHits", nUTHits );
  tree->Branch( "UT_cos", UT_cos );
  tree->Branch( "UT_cosT", UT_cosT );
  tree->Branch( "UT_dxDy", UT_dxDy );
  tree->Branch( "UT_lhcbID", UT_lhcbID );
  tree->Branch( "UT_planeCode", UT_planeCode );
  tree->Branch( "UT_sinT", UT_sinT );
  tree->Branch( "UT_size", UT_size );
  tree->Branch( "UT_tanT", UT_tanT );
  tree->Branch( "UT_weight", UT_weight );
  tree->Branch( "UT_xAtYEq0", UT_xAtYEq0 );
  tree->Branch( "UT_xAtYMid", UT_xAtYMid );
  tree->Branch( "UT_xMax", UT_xMax );
  tree->Branch( "UT_xMin", UT_xMin );
  tree->Branch( "UT_xT", UT_xT );
  tree->Branch( "UT_yBegin", UT_yBegin );
  tree->Branch( "UT_yEnd", UT_yEnd );
  tree->Branch( "UT_yMax", UT_yMax );
  tree->Branch( "UT_yMid", UT_yMid );
  tree->Branch( "UT_yMin", UT_yMin );
  tree->Branch( "UT_zAtYEq0", UT_zAtYEq0 );

  return sc;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode PrTrackRecoDumper::finalize() {

  StatusCode sc = Consumer::finalize();
  if ( sc.isFailure() ) return sc;

  file->Write();
  file->Close();

  return sc;
}

//=============================================================================
// operator()
//=============================================================================
void PrTrackRecoDumper::operator()( const Tracks& recTracks, const LHCb::VPLightClusters& VPClusters,
                                    const LHCb::ODIN& odin, const PrFTHitHandler<PrHit>& prFTHitHandler,
                                    const UT::HitHandler& prUTHitHandler ) const {

  verbose() << "Starting to dump..." << endmsg;

  verbose() << "Track" << endmsg;

  *eventID = odin.eventNumber();

  verbose() << "Loop on tracks" << endmsg;

  for ( const auto& track : recTracks ) {
    // Here we retireve the link between MC particles and reconstructed tracks
    std::string track_location = m_track_location;

    LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink( evtSvc(), msgSvc(), track_location );
    const LHCb::MCParticle*                 mcSeedPart = mySeedLink.first( &track );

    // Information of reconstructed track

    *p   = track.p();
    *px  = track.momentum().x();
    *py  = track.momentum().y();
    *pz  = track.momentum().z();
    *pt  = track.pt();
    *eta = track.pseudoRapidity();
    *phi = track.phi();
    *pid = 0; // track.particleID().pid(); //offline you want to match the PID eventually to the e+, e- or whatever
    *fromBeautyDecay  = false;
    *fromCharmDecay   = false;
    *fromStrangeDecay = false;
    *ovtx_x           = track.firstState().position().x();
    *ovtx_y           = track.firstState().position().y();
    *ovtx_z           = track.firstState().position().z();
    *key              = track.key();
    *ghostProb        = track.ghostProbability();
    *chi2             = track.chi2();
    *ndof             = track.nDoF();

    if ( mcSeedPart != 0 ) {
      *isMatched = true;

    } else {
      *isMatched = false;
    }

    // Velo
    *nVeloHits = 0;
    Velo_x->clear();
    Velo_y->clear();
    Velo_z->clear();
    Velo_Module->clear();
    Velo_Sensor->clear();
    Velo_Station->clear();
    Velo_lhcbID->clear();

    // SciFi
    *nFTHits = 0;
    FT_hitz->clear();
    FT_hitx->clear();
    FT_hitw->clear();
    FT_hitPlaneCode->clear();
    FT_hitzone->clear();
    FT_hitDXDY->clear();
    FT_hitYMin->clear();
    FT_hitYMax->clear();
    FT_lhcbID->clear();

    // UT
    *nUTHits = 0;
    UT_cos->clear();
    UT_cosT->clear();
    UT_dxDy->clear();
    UT_lhcbID->clear();
    UT_planeCode->clear();
    UT_sinT->clear();
    UT_size->clear();
    UT_tanT->clear();
    UT_weight->clear();
    UT_xAtYEq0->clear();
    UT_xAtYMid->clear();
    UT_xMax->clear();
    UT_xMin->clear();
    UT_xT->clear();
    UT_yBegin->clear();
    UT_yEnd->clear();
    UT_yMax->clear();
    UT_yMid->clear();
    UT_yMin->clear();
    UT_zAtYEq0->clear();

    auto ids = track.lhcbIDs();
    for ( auto& id : ids ) {
      if ( id.isVP() ) {
        auto vp_ID   = id.vpID();
        *nVeloHits   = *nVeloHits + 1;
        bool foundID = false;
        for ( auto& vphit : VPClusters )
          if ( vphit.channelID() == vp_ID.channelID() ) {
            foundID = true;
            Velo_x->push_back( vphit.x() );
            Velo_y->push_back( vphit.y() );
            Velo_z->push_back( vphit.z() );
            Velo_Module->push_back( vp_ID.module() );
            Velo_Sensor->push_back( vp_ID.sensor() );
            Velo_Station->push_back( vp_ID.station() );

            Velo_lhcbID->push_back( vp_ID.channelID() );
            break;
          }

        if ( !foundID ) error() << "Hit not found: " << vp_ID.channelID() << endmsg;
      }
      if ( id.isFT() ) {
        auto ft_ID   = id.ftID();
        *nFTHits     = *nFTHits + 1;
        bool foundID = false;
        for ( unsigned int zone = 0; PrFTInfo::nbZones() > zone; ++zone ) {
          for ( const auto& fthit : prFTHitHandler.hits( zone ) ) {
            if ( fthit.id().channelID() == ft_ID.channelID() ) {
              foundID = true;
              FT_hitz->push_back( fthit.z() );
              FT_hitx->push_back( fthit.x() );
              FT_hitw->push_back( fthit.w() );
              FT_hitPlaneCode->push_back( fthit.planeCode() );
              FT_hitzone->push_back( fthit.zone() );
              FT_hitDXDY->push_back( fthit.dxDy() );
              FT_hitYMin->push_back( fthit.yMin() );
              FT_hitYMax->push_back( fthit.yMax() );
              FT_lhcbID->push_back( fthit.id().channelID() );
              break;
            }
          }
        }
        if ( !foundID ) error() << "Hit not found: " << ft_ID.channelID() << endmsg;
      }

      if ( id.isUT() ) {
        auto ut_ID   = id.utID();
        bool foundID = false;
        *nUTHits     = *nUTHits + 1;
        for ( int iStation = 1; iStation < 3; ++iStation ) {
          for ( int iLayer = 1; iLayer < 3; ++iLayer ) {
            for ( int iRegion = 1; iRegion < 4; ++iRegion ) {
              for ( int iSector = 1; iSector < 99; ++iSector ) {
                for ( auto& uthit : prUTHitHandler.hits( iStation, iLayer, iRegion, iSector ) ) {
                  if ( uthit.chanID().channelID() == ut_ID.channelID() ) {
                    foundID = true;
                    UT_cos->push_back( uthit.cos() );
                    UT_cosT->push_back( uthit.cosT() );
                    UT_dxDy->push_back( uthit.dxDy() );
                    UT_lhcbID->push_back( uthit.chanID().channelID() );
                    UT_planeCode->push_back( uthit.planeCode() );
                    UT_sinT->push_back( uthit.sinT() );
                    UT_size->push_back( uthit.size() );
                    UT_tanT->push_back( uthit.tanT() );
                    UT_weight->push_back( uthit.weight() );
                    UT_xAtYEq0->push_back( uthit.xAtYEq0() );
                    UT_xAtYMid->push_back( uthit.xAtYMid() );
                    UT_xMax->push_back( uthit.xMax() );
                    UT_xMin->push_back( uthit.xMin() );
                    UT_xT->push_back( uthit.xT() );
                    UT_yBegin->push_back( uthit.yBegin() );
                    UT_yEnd->push_back( uthit.yEnd() );
                    break;
                  }
                }
              }
            }
          }
        }

        if ( !foundID ) error() << "Hit not found: " << ut_ID.channelID() << endmsg;
      }
    }

    tree->Fill();
  }
}
