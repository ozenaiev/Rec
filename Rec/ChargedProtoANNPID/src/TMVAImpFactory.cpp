/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "TMVAImpFactory.h"

namespace ANNGlobalPID {

  // Standard constructor
  TMVAImpFactory::TMVAImpFactory() {
    // Add the MC12TuneV2 implementations to the factory
    addMC12TuneV2();
    // Add the MC12TuneV3 implementations to the factory
    addMC12TuneV3();
    // Add the MC12TuneV4 implementations to the factory
    addMC12TuneV4();
    // Bs2MuMu developmental networks
    addBs2MuMuDev1();
    addBs2MuMuDev2();
    // MC 2015 neworks
    addMC15TuneV1();
    // MC Upgrade neworks
    addMCUpTuneV1();
  }

  // Method to get a static instance of the factory
  const TMVAImpFactory& tmvaFactory() {
    static TMVAImpFactory factory;
    return factory;
  }

} // namespace ANNGlobalPID
