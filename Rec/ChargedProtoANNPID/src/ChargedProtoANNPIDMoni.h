/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-------------------------------------------------------------------------------
/** @file ChargedProtoANNPIDMoni.h
 *
 *  Declaration file for ANN Combined PID algorithm ANNGlobalPID::ChargedProtoANNPIDMoni
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   12/02/2012
 */
//-------------------------------------------------------------------------------

#ifndef CHARGEDPROTOANNPIDMONI_H
#define CHARGEDPROTOANNPIDMONI_H 1

// local
#include "ChargedProtoANNPIDAlgBase.h"

namespace ANNGlobalPID {

  /** @class ChargedProtoANNPIDMoni ChargedProtoANNPIDMoni.h
   *
   *  Monitor for the ANNPID
   *
   *  @author Chris Jones
   *  @date   2012-01-12
   */

  class ChargedProtoANNPIDMoni final : public ChargedProtoANNPIDAlgBase {

  public:
    /// Standard constructor
    ChargedProtoANNPIDMoni( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~ChargedProtoANNPIDMoni() = default; ///< Destructor

    StatusCode execute() override; ///< Algorithm execution
  };

} // namespace ANNGlobalPID

#endif // CHARGEDPROTOANNPIDMONI_H
