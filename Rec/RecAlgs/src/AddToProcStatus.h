/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ADDTOPROCSTATUS_H
#define ADDTOPROCSTATUS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class AddToProcStatus AddToProcStatus.h
 *
 *  Add an entry to ProcStatus if some sequeence failed
 *
 *  @author Patrick Koppenburg
 *  @date   2011-06-15
 */
class AddToProcStatus final : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_subsystem{this, "Subsystem", "", "Subsystem that has aborted"}; ///< subsystem
  Gaudi::Property<std::string> m_reason{this, "Reason", "", "Reason"};                           ///< Reason for error
  Gaudi::Property<int>         m_status{this, "Status", -3, "Status Code"};                      ///< return code
  Gaudi::Property<bool>        m_abort{this, "Abort", true, "Abort Event?"}; ///< should processing be aborted?
};

#endif // ADDTOPROCSTATUS_H
