/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddBremInfo.h
 *
 * Header file for algorithm ChargedProtoParticleAddBremInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_ChargedProtoParticleAddBremInfo_H
#define GLOBALRECO_ChargedProtoParticleAddBremInfo_H 1

// from Gaudi
#include "ChargedProtoParticleCALOBaseAlg.h"

/** @class ChargedProtoParticleAddBremInfo ChargedProtoParticleAddBremInfo.h
 *
 *  Updates the CALO 'BREM' information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class ChargedProtoParticleAddBremInfo final : public ChargedProtoParticleCALOBaseAlg {

public:
  /// Standard constructor
  ChargedProtoParticleAddBremInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  /// Load the Calo Brem tables
  bool getBremData();

  /// Add Calo Brem information to the given ProtoParticle
  bool addBrem( LHCb::ProtoParticle* proto ) const;

private:
  std::string m_protoPath; ///< Location of the ProtoParticles in the TES

  std::string m_inBremPath;
  std::string m_bremMatchPath;
  std::string m_bremChi2Path;
  std::string m_bremPIDePath;

  const LHCb::Calo2Track::ITrAccTable*    m_InBremTable   = nullptr;
  const LHCb::Calo2Track::IHypoTrTable2D* m_bremTrTable   = nullptr;
  const LHCb::Calo2Track::ITrEvalTable*   m_BremChi2Table = nullptr;
  const LHCb::Calo2Track::ITrEvalTable*   m_dlleBremTable = nullptr;
};

#endif // GLOBALRECO_ChargedProtoParticleAddBremInfo_H
