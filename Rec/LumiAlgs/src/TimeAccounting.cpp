/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/IRegistry.h"

// event model
#include "Event/ODIN.h"
#include "Event/RawEvent.h"
#include "Event/TimeSpanFSR.h"

// local
#include "TimeAccounting.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TimeAccounting
//
// 2009-01-19 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TimeAccounting )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TimeAccounting::TimeAccounting( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"RawEventLocation", LHCb::RawEventLocation::Default},
                 KeyValue{"ODINLocation", LHCb::ODINLocation::Default}} ) {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode TimeAccounting::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  // get the File Records service
  m_fileRecordSvc = service( "FileRecordDataSvc", true );

  // prepare TDS for FSR
  m_timeSpanFSRs = new LHCb::TimeSpanFSRs();
  m_timeSpanFSR  = 0;
  put( m_fileRecordSvc.get(), m_timeSpanFSRs, m_FSRName );
  // create a new FSR and append to TDS
  m_timeSpanFSR = new LHCb::TimeSpanFSR();
  m_timeSpanFSRs->insert( m_timeSpanFSR );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void TimeAccounting::operator()( const LHCb::RawEvent& event, const LHCb::ODIN& odin ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // registry from raw data - only correct if file catalogue used
  const IOpaqueAddress* eAddr = event.registry()->address();
  // obtain the fileID
  if ( !eAddr ) {
    throw GaudiException( "Registry cannot be loaded from Event", "TimeAccounting" + name(), StatusCode::SUCCESS );
  }
  std::string event_fname = eAddr->par()[0];
  if ( msgLevel( MSG::DEBUG ) ) debug() << "RunInfo record from Event: " << event_fname << endmsg;

  // obtain the run number from ODIN
  unsigned int run = odin.runNumber();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "ODIN RunNumber: " << run << endmsg;

  // add file to list of seen files
  m_files.with_lock( [&event_fname]( std::unordered_set<std::string>& files ) { files.insert( event_fname ); } );
  // get the event time
  ulonglong eventTime = odin.gpsTime();
  // enter new event time
  ( *m_timeSpanFSR ) += eventTime;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode TimeAccounting::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;

  // some printout of FSRs
  if ( msgLevel( MSG::INFO ) ) {
    info() << "number of files seen: " << m_files.with_lock( &std::unordered_set<std::string>::size ) << endmsg;
    // FSR - use the class method which prints it
    for ( const auto& fsr : *m_timeSpanFSRs ) {
      // print the individual FSR
      info() << "FSR: " << *fsr << endmsg;
    }
  }

  // check if the FSRs can be retrieved from the TS
  if ( msgLevel( MSG::DEBUG ) ) {
    LHCb::TimeSpanFSRs* readFSRs = get<LHCb::TimeSpanFSRs>( m_fileRecordSvc.get(), m_FSRName );
    for ( const auto& fsr : *readFSRs ) {
      // print the FSR just retrieved from TS
      debug() << "READ FSR: " << *fsr << endmsg;
    }
  }

  return Consumer::finalize(); // must be called after all other actions
}
