#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  The set of basic objects from LoKiTrack library
#
#        This file is a part of LoKi project -
#    "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV, Sascha Stahl
# =============================================================================
"""
The set of basic objects from LoKiTrack library

      This file is a part of LoKi project -
``C++ ToolKit  for Smart and Friendly Physics Analysis''

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.
"""

# Namespaces:
import cppyy
cppyy.gbl.gSystem.Load('libLoKiTrack_v2Lib.so')
cppyy.gbl.gSystem.Load('libLoKiCoreLib.so')
cppyy.gbl.gSystem.Load('libTrackEvent.so')
cppyy.gbl.gInterpreter.Declare('#include "Event/Track_v2.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi_v2/Track.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi_v2/TrackTypes.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi_v2/ITrackFunctorFactory.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi_v2/ITrackFunctorAntiFactory.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi_v2/TrackEngine.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi_v2/LoKiTrack_dct.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi/Operators.h"')
cppyy.gbl.gInterpreter.Declare('#include "LoKi/Primitives.h"')

LoKi = cppyy.gbl.LoKi
LHCb = cppyy.gbl.LHCb
Track = LoKi.Pr.Track
Flag = LHCb.Event.v2.Enum.Track.Flag
#  workaround for cppyy enum 'feature' -- see https://sft.its.cern.ch/jira/browse/ROOT-8935
try:
    _dummy_ = Flag.Backward
except AttributeError:
    Flag = LHCb.Event.v2.Enum.Track

TrackType = LoKi.Pr.TrackType
import LoKiCore.decorators as _LoKiCore

_T = 'LHCb::Event::v2::Track const*'

# =============================================================================
## "Ordinary" functions for Track: "Tr"
# =============================================================================

## @see LoKi::Types::TrFunc
TrFunc = LoKi.Functor(_T, 'double')
## @see LoKi::Types::TrCuts
TrCuts = LoKi.Functor(_T, bool)
## @see LoKi::Types::TrFun
TrFun = LoKi.FunctorFromFunctor(_T, 'double')
## @see LoKi::Types::TrCut
TrCut = LoKi.FunctorFromFunctor(_T, bool)

## @see @see LoKi::Cuts::TrALL
TrALL = LoKi.Constant(_T, bool)(True)
## @see LoKi::Cuts::TrP
TrP = Track.Momentum()
## @see LoKi::Cuts::TrPT
TrPT = Track.TransverseMomentum()
## @see LoKi::Cuts::TrISFLAG
TrISFLAG = Track.CheckFlag
## Specializations
TrBACKWARD = Track.CheckFlag(Flag.Backward)
TrINVALID = Track.CheckFlag(Flag.Invalid)
## @see LoKi::Cuts::TrCHI2
TrCHI2 = Track.Chi2()
## @see LoKi::Cuts::TrCHI2PDOF
TrCHI2PDOF = Track.Chi2PerDoF()
## @see Impact parameter functors
TrMINIP = Track.MinimalImpactParameter
TrMINIPCUT = Track.MinimalImpactParameterGreaterThan

## @see LoKi::Track::MinimalImpactParameterChi2
TrMINIPCHI2 = Track.MinimalImpactParameterChi2

## @see LoKi::Track::MinimalImpactParameterChi2Cut
TrMINIPCHI2CUT = Track.MinimalImpactParameterChi2Cut
