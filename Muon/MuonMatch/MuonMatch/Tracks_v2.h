/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCH_TRACKS_H
#define MUONMATCH_TRACKS_H 1

// STL
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// LHCb
#include "Event/PrUpstreamTracks.h"
#include "Event/RecVertex_v2.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "PrKernel/PrSelection.h"

namespace MuonMatch {

  /// Track aliases
  using Track          = LHCb::Event::v2::Track;
  using Tracks         = std::vector<Track>;
  using TrackSelection = Pr::Selection<Track>;
  using Vertex         = LHCb::Event::v2::RecVertex;
  using Vertices       = std::vector<Vertex>;
  using TracksUT       = LHCb::Pr::Upstream::Tracks;
  using simd           = SIMDWrapper::scalar::types;

  inline LHCb::State get_state( const TracksUT& tracks, int t ) {
    LHCb::State state;

    const auto pos = tracks.statePos<simd::float_v>( t );
    const auto dir = tracks.stateDir<simd::float_v>( t );
    const auto cov = tracks.stateCov<simd::float_v>( t );

    const auto qop = tracks.stateQoP<simd::float_v>( t );

    state.setX( pos.x.cast() );
    state.setY( pos.y.cast() );
    state.setZ( pos.z.cast() );
    state.setTx( dir.x.cast() );
    state.setTy( dir.y.cast() );
    state.setQOverP( qop.cast() );

    state.covariance()( 0, 0 ) = cov.x.cast();
    state.covariance()( 2, 0 ) = cov.y.cast();
    state.covariance()( 2, 2 ) = cov.z.cast();
    state.covariance()( 1, 1 ) = cov.x.cast();
    state.covariance()( 3, 1 ) = cov.y.cast();
    state.covariance()( 3, 3 ) = cov.z.cast();
    state.covariance()( 4, 4 ) = 1.f;

    return state;
  }

  /// Calculate the squared impact parameter of a track with respect to a vertex
  template <class TRACK = Track> // CHANGE THIS WHEN TRACK VERSION IS SET
  inline auto squared_impact_parameter( const TRACK& track, const Vertex& vertex ) {
    const auto& pos   = vertex.position();
    const auto& state = track.closestState( pos.Z() );
    const auto  line  = Gaudi::Math::Line{state.position(), state.slopes()};
    const auto  ipvec = Gaudi::Math::closestPoint( pos, line ) - pos;

    return ipvec.Mag2();
  }

  /// Determine the track charge
  inline int track_charge( const LHCb::State& state ) {
    float qP = state.qOverP();
    return UNLIKELY( std::abs( qP ) < TrackParameters::lowTolerance ) ? 0 : qP < 0 ? -1 : +1;
  }

  /// Calculate the projection in the magnet for the "x" axis
  template <class value>
  inline auto xStraight( const LHCb::State& state, value z ) {
    return state.x() + ( z - state.z() ) * state.tx();
  }

  /// Calculate the projection in the magnet for the "y" axis
  template <class value>
  inline auto yStraight( const LHCb::State& state, value z ) {
    return state.y() + ( z - state.z() ) * state.ty();
  }

  /// Calculate the projection in the magnet for the "y" axis
  template <class value>
  inline std::tuple<value, value> yStraightWindow( const LHCb::State& state, value z, value yw ) {
    const auto dz = z - state.z();
    const auto y  = state.y() + dz * state.ty();
    const auto r  = dz * std::sqrt( state.errTy2() ) + yw;

    return {y - r, y + r};
  }

} // namespace MuonMatch

#endif // MUONMATCH_TRACKS_H
